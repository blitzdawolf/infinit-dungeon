﻿using infdungeon.objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AITrainer : MonoBehaviour
{
    public List<AICharacter> characters = new List<AICharacter>();

    public Dictionary<AICharacter, Brain> AiBrains = new Dictionary<AICharacter, Brain>();

    public TuturialRoomCreator trc;

    public Brain b;
    public bool randomNetworkSize = false;

    public int[] network;

    public float[] input;
    public float[] outupt;

    public bool update = false;
    public bool mutate = false;

    public QueueUI qUI;

    public Vector2Int centerPos;
    public GameObject go;
    public GameObject test;

    [Range(1,11)]
    public int searchSize = 5;

    public string saved = "";

    void Start()
    {
        
        Tile centerTile = trc.myRoom.getTile(centerPos.x, centerPos.y);

        qUI.currentRoom = trc.myRoom;

        input = new float[searchSize * searchSize];
        network[0] = searchSize * searchSize;

        if (randomNetworkSize)
        {
            for (int i = 1; i < network.Length - 1; i++)
            {
                network[i] = Random.Range(3, 10);
            }
        }

        if(CharacterSelection.Ins != null)
        {
            CharacterSelection.Ins.myCharacters.ForEach(item =>
            {
                PlayerCharacter pc = Instantiate(item.PlayerCharacter);
                pc.CurrentRoom = trc.myRoom;
                pc.RoomPosition = new Vector2Int((int)Random.Range(1, trc.myRoom.Width - 1), (int)Random.Range(1, trc.myRoom.Width - 1));
                // pc.transform.SetParent(GameObject.Find("charactersHolder").transform);
                ChracterManager.Ins.AddChracter(pc);
            });
        }

        /*foreach (AICharacter item in characters)
        {
            ChracterManager.Ins.AddChracter(item);
        }*/
        // ChracterManager.Ins.CreateQueue(trc.myRoom);

        b = new Brain(network);
        if (saved != "")
        {
            b.FromJson(saved);
        }
        else
        {
            saved = b.ToJson();
            Debug.Log(JsonUtility.ToJson(b));
        }
    }

    public IEnumerator GetView()
    {
        int low = (searchSize - 1) / 2;
        for (int i = 0; i < Camera.main.transform.childCount; i++)
        {
            GameObject.Destroy(Camera.main.transform.GetChild(i).gameObject);
        }

        for (int x = -low; x <= low; x++)
        {
            for (int y = -low; y <= low; y++)
            {
                Tile t = trc.myRoom.getTile(x + centerPos.x, y + centerPos.y);
                int st = (x + low) * searchSize + (y + low);
                if (t == null || t.wall)
                {
                    input[st] = 1;
                    Instantiate(test, new Vector3(x + centerPos.x, y + centerPos.y, 0), Quaternion.identity).transform.SetParent(Camera.main.transform);
                }
                else
                {
                    // AStar star = new AStar(trc.myRoom, trc.myRoom.getTile(centerPos.x, centerPos.y), t);
                    input[st] = 0;
                    if (HitsWall(t))//!star.ValidPath || star.PathLength > ((searchSize - 1) / 2) + 1)
                    {
                        input[st] = 1;
                        Instantiate(test, new Vector3(x + centerPos.x, y + centerPos.y, 0), Quaternion.identity).transform.SetParent(Camera.main.transform);
                    }
                }
            }
        }
        yield return null;
    }

    public bool HitsWall(Tile start)
    {
        int x = start.X;
        int y = start.Y;

        Tile currentTile = start;
        Tile destination = trc.myRoom.getTile(centerPos.x, centerPos.y);
        List<Tile> cameFrom = new List<Tile>();
        while (currentTile != destination)
        {
            float lowest = float.MaxValue;
            Tile best = null;
            List<Tile> tiles = start.GetNeighbours(true).ToList();
            foreach (var item in tiles)
            {
                if (item == currentTile)
                    continue;
                if (cameFrom.Contains(item))
                    continue;
                if (item == null)
                {
                    Debug.Log($"Tile is empty");
                    continue;
                }
                if (Vector2.Distance(new Vector2(item.X, item.Y), new Vector2(destination.X, destination.Y)) < lowest)
                {
                    lowest = Vector2.Distance(new Vector2(item.X, item.Y), new Vector2(destination.X, destination.Y));
                    best = item;
                }
            }
            if (best == null)
                return false;
            if (best.wall)
            {
                Debug.Log($"The tile is a wall, X:{best.X}, Y:{best.Y}");
                return true;
            }
            currentTile = best;
            cameFrom.Add(best);
        }

        return false;
    }

    public void FloodFill(Tile[,] tiles)
    {
        Tile startTile = tiles[(searchSize - 1) / 2, (searchSize - 1) / 2];
        Dictionary<Tile, Tuple<bool,float>> ClosedSet = new Dictionary<Tile, Tuple<bool, float>>();
        ClosedSet.Add(startTile, new Tuple<bool, float>(true, 0));
        List<Tile> openSet = startTile.GetNeighbours().ToList();
        while (ClosedSet.Count < searchSize * searchSize)
        {
            foreach (var item in openSet)
            {
                if (ClosedSet.ContainsKey(item))
                {
                    continue;
                }
                ClosedSet.Add(item, new Tuple<bool, float>(item.wall, Vector2.Distance(new Vector2(item.X, item.Y), new Vector2(startTile.X, startTile.Y))));
                foreach (var jtem in item.GetNeighbours())
                {
                    if (jtem == item)
                        continue;
                    if (ClosedSet.ContainsKey(jtem))
                    {
                        if (ClosedSet[jtem].Item2 < Vector2.Distance(new Vector2(item.X, item.Y), new Vector2(startTile.X, startTile.Y)))
                        {
                            if (ClosedSet[jtem].Item1)
                            {

                            }
                        }
                    }
                }
            }
        }
    }

    public void CalculateNetwork()
    {
        outupt = b.FeedForward(input);
        
    }
    
    void Update()
    {
        /* centerPos.x = (int)go.transform.position.x;
        centerPos.y = (int)go.transform.position.y;
        StartCoroutine(GetView()); */
        if (update)
        {
            CalculateNetwork();
            update = false;
        }
        if (mutate)
        {
            b.Mutate();
            mutate = false;
        }
    }
}