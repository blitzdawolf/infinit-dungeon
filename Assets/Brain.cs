﻿using System.Collections.Generic;
using System;
using UnityEngine;
using Random = System.Random;

[System.Serializable]
public class Brain
{
    public int[] layers;
    public float[][] neurons;
    public float[][][] weights;

    Random rnd;

    public Brain(int[] layers)
    {
        this.layers = new int[layers.Length];
        for (int i = 0; i < layers.Length; i++)
        {
            this.layers[i] = layers[i];
        }

        rnd = new Random(System.DateTime.Today.Millisecond);

        InitNeurons();
        InitWeights();
    }

    private void InitNeurons()
    {
        List<float[]> neuronsList = new List<float[]>();

        for (int i = 0; i < layers.Length; i++)
        {
            neuronsList.Add(new float[layers[i]]);
        }

        neurons = neuronsList.ToArray();
    }

    private void InitWeights()
    {
        List<float[][]> weightsList = new List<float[][]>();

        for (int i = 1; i < layers.Length; i++)
        {
            List<float[]> layerWeightList = new List<float[]>();

            int neuronsInPreviosLayer = layers[i - 1];

            for (int j = 0; j < neurons[i].Length; j++)
            {
                float[] neuromWeights = new float[neuronsInPreviosLayer];

                for (int k = 0; k < neuronsInPreviosLayer; k++)
                {
                    neuromWeights[k] = (float)rnd.NextDouble() - 0.5f;
                }

                layerWeightList.Add(neuromWeights);
            }

            weightsList.Add(layerWeightList.ToArray());
        }

        weights = weightsList.ToArray();
    }

    public float[] FeedForward(float[] inputs)
    {
        for (int i = 0; i < inputs.Length; i++)
        {
            neurons[0][i] = inputs[i];
        }

        for (int i = 1; i < layers.Length; i++)
        {
            for (int j = 0; j < neurons[i].Length; j++)
            {
                float value = 0.25f;
                for (int k = 0; k < neurons[i - 1].Length; k++)
                {
                    value += weights[i - 1][j][k] * neurons[i - 1][k];
                }

                neurons[i][j] = (float)Math.Tanh(value);
            }
        }

        return neurons[neurons.Length - 1];
    }

    public void Mutate()
    {
        for (int i = 0; i < weights.Length; i++)
        {
            for (int j = 0; j < weights[i].Length; j++)
            {
                for (int k = 0; k < weights[i][j].Length; k++)
                {
                    float weight = weights[i][j][k];

                    float randomNumber = (float)rnd.NextDouble() * 1000f;

                    if (randomNumber <= 2f)
                        weight *= -1f;
                    else if (randomNumber <= 4f)
                        weight = UnityEngine.Random.Range(-.5f, 0.5f);
                    else if (randomNumber < 6f)
                    {
                        float factor = UnityEngine.Random.Range(0f, 1f) + 1f;
                        weight *= factor;
                    }
                    else if (randomNumber < 8f)
                    {
                        float factor = UnityEngine.Random.Range(0f, 1f);
                        weight *= factor;
                    }

                    weights[i][j][k] = weight;
                }
            }
        }
    }

    public string ToJson()
    {
        JsonBrain jb = new JsonBrain();
        jb.Layers = JsonHelper.ToJson(layers);


        List<string> b = new List<string>();
        for (int i = 0; i < weights.Length; i++)
        {
            List<string> a = new List<string>();
            for (int j = 0; j < weights[i].Length; j++)
            {
                a.Add(JsonHelper.ToJson(weights[i][j], true));
            }
            b.Add(JsonHelper.ToJson(a.ToArray(), true));
        }
        jb.weights = JsonHelper.ToJson(b.ToArray(), true);

        b = new List<string>();
        for (int i = 0; i < neurons.Length; i++)
        {
            b.Add(JsonHelper.ToJson(neurons[i]));
        }
        jb.neurons = JsonHelper.ToJson(b.ToArray(), true);

        return JsonUtility.ToJson(jb, true);
    }

    public void FromJson(string str)
    {
        JsonBrain jb = JsonUtility.FromJson<JsonBrain>(str);

        layers = JsonHelper.FromJson<int>(jb.Layers);
        List<float[]> neuronsList = new List<float[]>();
        for (int i = 0; i < layers.Length; i++)
        {
            neuronsList.Add(JsonHelper.FromJson<float>(jb.neurons));            
        }
        neurons = neuronsList.ToArray();

        List<float[][]> weightsList = new List<float[][]>();
        string[] weights = JsonHelper.FromJson<string>(jb.weights);
        for (int i = 0; i < weights.Length; i++)
        {
            string[] aWeights = JsonHelper.FromJson<string>(weights[i]);
            List<float[]> w = new List<float[]>();
            for (int j = 0; j < aWeights.Length; j++)
            {
                w.Add(JsonHelper.FromJson<float>(aWeights[j]));
            }
            weightsList.Add(w.ToArray());
        }
        this.weights = weightsList.ToArray();
    }

    public struct JsonBrain
    {
        public string Layers;
        public string weights;
        public string neurons;
    }
}
