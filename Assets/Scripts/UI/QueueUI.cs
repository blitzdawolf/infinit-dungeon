﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueueUI : MonoBehaviour
{
    public Transform Parent;
    public GameObject prefab;

    public Room currentRoom;
    public List<ICharacter> Characters = new List<ICharacter>();

    private void Start()
    {
    }

    public void Test()
    {
        // GetQueue
        // GetCharacterInRoom
        int i = 0;
        var placeHolder = ChracterManager.Ins.GetQueue(currentRoom);
        if (placeHolder == null)
            return;
        if(true)
        {
            Characters = placeHolder;
            while (Parent.childCount > 0)
            {
                GameObject obj = Parent.GetChild(0).gameObject;
                obj.transform.parent = null;
                Destroy(obj);
            }
            // placeHolder.Reverse();
            foreach (var item in placeHolder)
            {
                if (item.Down)
                    continue;

                string prefix = "Player - ";
                if (item.GetType() == typeof(AICharacter))
                    prefix = "AI - ";
                GameObject o = GameObject.Instantiate(prefab);

                if (item.GetType() == typeof(PlayerCharacter))
                    o.GetComponentInChildren<Text>().color = Color.green;

                o.GetComponentInChildren<Text>().text = prefix + item.Name;
                o.GetComponentInChildren<Image>().sprite = ((Character)item).GetComponentInChildren<SpriteRenderer>().sprite;
                o.GetComponentInChildren<Image>().color = ((Character)item).GetComponentInChildren<SpriteRenderer>().color;
                o.transform.SetParent(Parent);
                o.name = item.Name;
                i++;
            }
        }
    }

    private void Update()
    {
        if(currentRoom != null)
        {
            Test();
        }
    }
}
