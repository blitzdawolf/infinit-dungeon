﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour
{
    public GameObject TinyHealthBar;
    public ICharacter character;

    private void Start()
    {
        character = GetComponent<ICharacter>();
    }

    private void Update()
    {
        TakeDamage();
    }

    public void TakeDamage()
    {
        TinyHealthBar.transform.localScale = new Vector3((character.HealthLeft / character.Health), 1, 1);
    }
}