﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ItemTestManager : MonoBehaviour
{
    public static readonly string SAVEPATH = Application.streamingAssetsPath + "/../Resources/items";

    [SerializeField]
    bool forceLoad;
    [Header("Items")]
    public List<Item>   items   = new List<Item>();
    public List<Armour> armours = new List<Armour>();
    public List<Weapon> weapons = new List<Weapon>();

    private void Start()
    {
        LoadJson();
    }

    public void SaveJson()
    {
        items.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });
        armours.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });
        weapons.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });

        string itemJson = JsonHelper.ToJson<Item>(items.ToArray(), true);
        string armourJson = JsonHelper.ToJson<Armour>(armours.ToArray(), true);
        string weaponJson = JsonHelper.ToJson<Weapon>(weapons.ToArray(), true);

        GameState.LoadCheckSum();

        GameState.SaveJson("items", itemJson);
        GameState.SaveJson("armour", armourJson);
        GameState.SaveJson("weapon", weaponJson);

        GameState.UnLoadCheckSum();
    }

    public void LoadJson()
    {
        string itemJson = Resources.Load<TextAsset>("items/items").text;
        string armourJson = Resources.Load<TextAsset>("items/armour").text;
        string weaponJson = Resources.Load<TextAsset>("items/weapon").text;
        Dictionary<string, string> checkSums = GameState.LoadCheckSum();

        if (GameState.MD5Hash(itemJson) == checkSums["items"] || forceLoad)
            items = JsonHelper.FromJson<Item>(itemJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
        if (GameState.MD5Hash(armourJson) == checkSums["armour"] || forceLoad)
            armours = JsonHelper.FromJson<Armour>(armourJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
        if (GameState.MD5Hash(weaponJson) == checkSums["weapon"] || forceLoad)
            weapons = JsonHelper.FromJson<Weapon>(weaponJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
    }
}
