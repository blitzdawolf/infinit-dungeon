﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemManager
{
    private static ItemManager _Ins;
    public static ItemManager Ins
    {
        get
        {
            if (_Ins != null)
                return _Ins;
            else
            {
                return new ItemManager();
            }
        }
        set
        {
            if (_Ins == null || _Ins == value)
            {
                _Ins = value;
            }
            else
            {
                Debug.Log("Value has already been set");
            }
        }
    }

    bool forceLoad = false;
    bool tryedLoading = false;

    public List<Item> items = new List<Item>();
    public List<Armour> armours = new List<Armour>();
    public List<Weapon> weapons = new List<Weapon>();

    public List<IItem> AllItems
    {
        get
        {
            List<IItem> i = new List<IItem>();
            i.AddRange(items);
            i.AddRange(armours);
            i.AddRange(weapons);
            if (i.Count == 0 && !tryedLoading)
            {
                LoadJson();
                tryedLoading = true;
                i = AllItems;
                tryedLoading = false;
            }
            return i;
        }
    }

    public ItemManager()
    {
        LoadJson();
    }

    public void SaveJson()
    {
        items.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });
        armours.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });
        weapons.ForEach(x => { if (string.IsNullOrWhiteSpace(x.ID)) { x.ID = Guid.NewGuid().ToString(); } });

        string itemJson = JsonHelper.ToJson<Item>(items.ToArray(), true);
        string armourJson = JsonHelper.ToJson<Armour>(armours.ToArray(), true);
        string weaponJson = JsonHelper.ToJson<Weapon>(weapons.ToArray(), true);

        GameState.LoadCheckSum();

        GameState.SaveJson("items", itemJson);
        GameState.SaveJson("armour", armourJson);
        GameState.SaveJson("weapon", weaponJson);

        GameState.UnLoadCheckSum();
    }

    public void LoadJson()
    {
        string itemJson = Resources.Load<TextAsset>("items/items").text;
        string armourJson = Resources.Load<TextAsset>("items/armour").text;
        string weaponJson = Resources.Load<TextAsset>("items/weapon").text;
        Dictionary<string, string> checkSums = GameState.LoadCheckSum();

        if (GameState.MD5Hash(itemJson) == checkSums["items"] || forceLoad)
            items = JsonHelper.FromJson<Item>(itemJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }

        if (GameState.MD5Hash(armourJson) == checkSums["armour"] || forceLoad)
            armours = JsonHelper.FromJson<Armour>(armourJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }

        if (GameState.MD5Hash(weaponJson) == checkSums["weapon"] || forceLoad)
            weapons = JsonHelper.FromJson<Weapon>(weaponJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
    }

    public IItem GetIteById(string ID)
    {
        return AllItems.Where(x => x.ID == ID).FirstOrDefault();
    }

    public IItem GetItemById(string ID, ItemType type)
    {
        switch (type)
        {
            case ItemType.Item:
                return items.Where(x => x.ID == ID).FirstOrDefault();
            case ItemType.Weapon:
                return weapons.Where(x => x.ID == ID).FirstOrDefault();
            case ItemType.Armour:
                return armours.Where(x => x.ID == ID).FirstOrDefault();
            default:
                return GetIteById(ID);
        }
    }

    public IItem GetItemByName(string name)
    {
        return AllItems.Where(x => x.Name == name).FirstOrDefault();
    }

    public enum ItemType
    {
        Item,
        Weapon,
        Armour
    }

    public Sprite loadSprite(IItem item)
    {
        Sprite[] sprite = Resources.LoadAll<Sprite>(item.SpriteName);
        return sprite[0];
    }
}
