﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NavigateManager
{
    bool saveToJSON = false;

    private static NavigateManager _Ins;

    public static NavigateManager Ins
    {
        get
        {
            if (_Ins != null)
                return _Ins;
            else
            {
                return new NavigateManager();
            }
        }
        set
        {
            if (_Ins == null || _Ins == value)
            {
                _Ins = value;
            }
            else
            {
                Debug.Log("Value has already been set");
            }
        }
    }

    private Dictionary<string, NavigateMenu> Menus = new Dictionary<string, NavigateMenu>();

    public NavigateManager()
    {
        Ins = this;
    }

    // Removes all windows that where in a other scene
    void checkForNullMenus() => Menus.Keys.ToList().ForEach(x => { if (Menus[x] == null) { Menus.Remove(x); } });

    public void AddMenu(NavigateMenu menu)
    {
        checkForNullMenus();
        if (Menus.Values.Where(x => x.defaultActive).Count() > 0)
            menu.defaultActive = false;
        Menus.Add(menu.menuName, menu);
        if (!menu.defaultActive)
            menu.gameObject.SetActive(false);

        NavigateMenu[] menus = Menus.Values.ToArray();
    }

    public void SwitchMenu(string name, NavigateMenu oldMenu)
    {
        if (Menus.ContainsKey(name))
        {
            Menus[name].gameObject.SetActive(true);
            Menus[name].cameFrom = oldMenu.menuName;
        }
        else
        {
            Menus[oldMenu.cameFrom].gameObject.SetActive(true);
        }
        oldMenu.gameObject.SetActive(false);
    }
}
