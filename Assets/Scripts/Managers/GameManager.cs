﻿public class GameManager
{
    public static GameManager Ins
    {
        get
        {
            if(ins == null)
            {
                ins = new GameManager();
            }
            return ins;
        }
    }
    private static GameManager ins;

    public float GameSpeed = 1f;
}