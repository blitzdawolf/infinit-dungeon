﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class CharacterExportManager : MonoBehaviour
    {
        public List<string> locations = new List<string>();
        public List<ICharacter> characters = new List<ICharacter>();

        public void SaveJson()
        {
            string json = JsonHelper.ToJson<string>(locations.ToArray());
            GameState.LoadCheckSum();
            GameState.SaveJson("characters", json);
            GameState.UnLoadCheckSum();
        }

        public void LoadJson()
        {

        }

        public void Load()
        {
            this.characters = new List<ICharacter>();
            List<string> copiedLocations = new List<string>(locations);
            foreach (string item in copiedLocations)
            {
                ICharacter test = Resources.Load<Character>("Characters/" + item);
                if (test != null && !this.characters.Contains(test))
                {
                    characters.Add(test);
                }
                else
                {
                    locations.Remove(item);
                }
            }
        }
    }
}
