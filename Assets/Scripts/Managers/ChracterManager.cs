﻿using System;
using System.Collections.Generic;
using System.Linq;
using infdungeon.objects;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class ChracterManager
{
    #region staticChracterController
    public static ChracterManager Ins
    {
        get
        {
            if (_Ins == null)
            {
                _Ins = new ChracterManager();
            }
            return _Ins;
        }
    }

    public static List<PlayerCharacter> GetAllPlayerCharacter()
    {
        List<PlayerCharacter> characters = new List<PlayerCharacter>();
        foreach (var item in GetAllCharacters().Where(x => x.GetType() == typeof(PlayerCharacter)).ToList())
        {
            characters.Add((PlayerCharacter)item);
        }
        return characters;
    }

    public Tuple<PlayerCharacter, int> getClosestPlayer(Tile current,Room currentRoom)
    {
        List<ICharacter> players =  dCharacters[currentRoom].Where(item => item.GetType() == typeof(PlayerCharacter)).ToList();
        int minLength = int.MaxValue;
        ICharacter clostest = null;
        players.ForEach(i =>
        {
            AStar a = new AStar(currentRoom, current, i.CurrentTile);
            if(minLength > a.PathLength)
            {
                minLength = a.PathLength;
                clostest = i;
            }
        });
        return new Tuple<PlayerCharacter, int>((PlayerCharacter)clostest, minLength);
    }

    private static ChracterManager _Ins;
    #endregion

    private List<ICharacter> characters = new List<ICharacter>();
    private Dictionary<Room, List<ICharacter>> dCharacters = new Dictionary<Room, List<ICharacter>>();
    public int GetCount(Room room)
    {
        if (dCharacters.ContainsKey(room))
        {
            return dCharacters[room].Count;
        }
        return -1;
    }
    private Dictionary<Room, int> roomQueue = new Dictionary<Room, int>();
    public Dictionary<Room, int> RoomQueue => roomQueue;

    public List<ICharacter> GetQueue(Room room)
    {
        if (roomQueue.ContainsKey(room))
        {
            List<ICharacter> chars = new List<ICharacter>(dCharacters[room]);
            for (int i = 0; i < roomQueue[room]; i++)
            {
                ListHelper<ICharacter>.ShiftRight(ref chars);
            }
            return chars;
        }
        return null;
    }

    private System.Action<PlayerCharacter> PlayerTurn;
    private System.Action<AICharacter> AITurn;

    public void ResetRoom(Room room)
    {
        dCharacters[room] = new List<ICharacter>();
    }

    public void RegisterPlayerTurn(System.Action<PlayerCharacter> action)
    {
        PlayerTurn += action;
    }

    public void RegisterAITurn(System.Action<AICharacter> action)
    {
        AITurn += action;
    }

    private ChracterManager()
    {

    }

    public bool AddChracter(ICharacter character)
    {
        if (character == null || character.CurrentRoom == null)
            return false;

        if (!characters.Contains(character))
        {
            characters.Add(character);
        }
        if (dCharacters.ContainsKey(character.CurrentRoom))
        {
            dCharacters[character.CurrentRoom].Add(character);
        }
        else
        {
            dCharacters.Add(character.CurrentRoom, new List<ICharacter>());
            dCharacters[character.CurrentRoom].Add(character);
        }
        return true;
    }

    public void CreateQueues()
    {
        List<Room> rooms = getRooms();
        foreach (Room room in rooms)
        {
            List<ICharacter> charactersInRoom = this.characters.Where(x => x.CurrentRoom == room).ToList();
            ListHelper<ICharacter>.Shuffle(ref charactersInRoom);
            roomQueue[room] = -1;
            dCharacters[room] = characters;
        }
    }

    public void UpdateCharacter(Character playerCharacter, Room lastRoom, Room currentRoom)
    {
        dCharacters[lastRoom].Remove(playerCharacter);
        AddChracter(playerCharacter);
        CreateQueue(lastRoom);
    }

    public void CreateQueue(Room room)
    {
        List<ICharacter> charactersInRoom = this.characters.Where(x => x.CurrentRoom == room).ToList();
        ListHelper<ICharacter>.Shuffle(ref charactersInRoom);
        roomQueue[room] = -1;
        dCharacters[room] = characters;
    }

    private List<Room> getRooms()
    {
        List<Room> foundRooms = new List<Room>();
        foreach (var item in this.characters)
        {
            if (!foundRooms.Contains(item.CurrentRoom))
            {
                foundRooms.Add(item.CurrentRoom);
            }
        }
        return foundRooms;
    }

    public ICharacter GetNextCharacter(Room room)
    {
        if (roomQueue.ContainsKey(room))
        {
            roomQueue[room]++;

            if (roomQueue[room] >= dCharacters[room].Count)
                roomQueue[room] = 0;

            if (dCharacters[room][roomQueue[room]].Down)
                return GetNextCharacter(room);

            ICharacter character = dCharacters[room][roomQueue[room]];
            return character;
        }
        return null;
    }

    public void nextTurn(Room room, bool doneOnce = false)
    {
        ICharacter character = GetNextCharacter(room);
        if (doneOnce)
        {
            CreateQueue(room);
            // return;
        }
        if (character == null)
        {
            nextTurn(room, true);
        }
        if(character.GetType() == typeof(PlayerCharacter))
        {
            PlayerTurn?.Invoke((PlayerCharacter)character);
        }
        else if(character.GetType() == typeof(AICharacter))
        {
            AITurn?.Invoke((AICharacter)character);
        }
        else
        {
            nextTurn(room);
        }
    }

    public static List<ICharacter> GetAllCharacters()
    {
        List<ICharacter> characters = new List<ICharacter>();
        List<string> charactersStrings = JsonHelper.FromJson<string>(Resources.Load<TextAsset>("items/characters").text).ToList();
        foreach (var item in charactersStrings)
        {
            characters.Add(Resources.Load<Character>("Characters/" + item));
        }
        return characters;
    }

    public List<ICharacter> GetCharacterInRoom(Room room)
    {
        return dCharacters[room];
    }

    public List<AICharacter> GetAiCharacters()
    {
        List<AICharacter> characters = new List<AICharacter>();
        foreach (var item in GetAllCharacters().Where(x => x.GetType() == typeof(AICharacter)).ToList())
        {
            characters.Add((AICharacter)item);
        }
        return characters;
    }

    public ICharacter GetCharcacterFromTile(Tile next, Room currentRoom)
    {
        return characters
            .Where(item => item.CurrentRoom == currentRoom)
            .Where(item => item.CurrentTile.Position == next.Position)
            .FirstOrDefault();
    }
}