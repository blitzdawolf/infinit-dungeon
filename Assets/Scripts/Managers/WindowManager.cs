﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WindowManager
{
    private static WindowManager _Ins;
    public static WindowManager Ins
    {
        get
        {
            if (_Ins != null)
                return _Ins;
            else
            {
                return new WindowManager();
            }
        }
        set
        {
            if (_Ins == null || _Ins == value)
            {
                _Ins = value;
            }
            else
            {
                Debug.Log("Value has already been set");
            }
        }
    }

    private Dictionary<string, Window> Windows = new Dictionary<string, Window>();
    private Queue<Window> openWindowQueue = new Queue<Window>();

    // Removes all windows that where in a other scene
    void checkForNullWindows() => Windows.Keys.ToList().ForEach(x => { if (Windows[x] == null) { Windows.Remove(x); } });

    public void AddWindow(Window window)
    {
        checkForNullWindows();
        Windows.Add(window.WindowName, window);
    }

    public bool WindowExists(string name)
    {
        if (Windows.ContainsKey(name))
        {
            Window w = Windows[name];
            if (w == null)
            {
                Windows.Remove(name);
            }
            else
            {
                return true;
            }
        }
        return false;
    }

    public void OpenWindow(string name)
    {
        if (WindowExists(name))
        {
            Window windowToOpen = Windows[name];
            if (!openWindowQueue.Contains(windowToOpen))
            {
                windowToOpen.gameObject.SetActive(true);
                openWindowQueue.Enqueue(windowToOpen);
            }
        }
    }

    public void closeLastWindow()
    {
        Window windowToClose = openWindowQueue.Dequeue();
        if(windowToClose.open)
            windowToClose.close();
    }

    public void closeWindow(string name)
    {
        if (WindowExists(name))
        {
            Window windowToClose = Windows[name];
            windowToClose.close();
        }
    }

    public WindowManager()
    {
        Ins = this;
    }
}
