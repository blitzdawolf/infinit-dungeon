﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomTestManager : MonoBehaviour
{
    [Tooltip("bypass the checksum")]
    [SerializeField]
    bool forceLoad = false;
    public List<Room> rooms = new List<Room>();

    private void Start()
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            GameObject go = Resources.Load<GameObject>("Rooms/" + rooms[i].Location);
            GameObject.Instantiate(go, Vector3.right * (i * 30), Quaternion.identity);
        }
    }

    public void SaveJson()
    {
        string itemJson = JsonHelper.ToJson<Room>(rooms.ToArray(), true);

        GameState.LoadCheckSum();

        GameState.SaveJson("rooms", itemJson);

        GameState.UnLoadCheckSum();
    }

    public void LoadJson()
    {
        string roomJson = Resources.Load<TextAsset>("items/rooms").text;
        Dictionary<string, string> checkSum = GameState.LoadCheckSum();

        if (GameState.MD5Hash(roomJson) == checkSum["rooms"] || forceLoad)
            rooms = JsonHelper.FromJson<Room>(roomJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
    }
}
