﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomManager
{
    private static RoomManager _Ins;
    public static RoomManager Ins
    {
        get
        {
            if (_Ins != null)
                return _Ins;
            else
            {
                return new RoomManager();
            }
        }
        set
        {
            if (_Ins == null || _Ins == value)
            {
                _Ins = value;
            }
            else
            {
                Debug.Log("Value has already been set");
            }
        }
    }

    bool forceLoad = false;
    public List<Room> rooms = new List<Room>();

    public RoomManager()
    {
        LoadJson();
    }

    public void SaveJson()
    {
        string itemJson = JsonHelper.ToJson<Room>(rooms.ToArray(), true);

        GameState.LoadCheckSum();

        GameState.SaveJson("rooms", itemJson);

        GameState.UnLoadCheckSum();
    }

    public void LoadJson()
    {
        string roomJson = Resources.Load<TextAsset>("items/rooms").text;
        Dictionary<string, string> checkSum = GameState.LoadCheckSum();

        if (GameState.MD5Hash(roomJson) == checkSum["rooms"] || forceLoad)
            rooms = JsonHelper.FromJson<Room>(roomJson).ToList();
        else
        {
            GameState.Development = true;
            forceLoad = true;
            LoadJson();
            return;
        }
    }

    public List<Room> GetRoomsBetween(int min, int max)
    {
        List<Room> r = rooms.Where(x => ( x.StartLevel >= min && x.EndLevel <= max)).ToList();
        return r;
    }

    public List<Room> GetLevelRoom(int level)
    {
        List<Room> r = rooms.Where(x => (level >= x.StartLevel && level <= x.EndLevel)).ToList();
        return r;
    }
}
