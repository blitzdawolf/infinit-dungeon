﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ItemTestManager))]
public class ItemTestManagerEditor : Editor
{ 

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Export"))
        {
            ((ItemTestManager)target).SaveJson();
        }
        if(GUILayout.Button("Load from JSON"))
        {
            ((ItemTestManager)target).LoadJson();
        }
    }
}
