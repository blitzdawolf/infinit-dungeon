﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DungeonGeneratorController))]
public class DungeonGeneratorControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DungeonGeneratorController dgc =(DungeonGeneratorController)target;
        if (dgc.dungeonGenerator != null) {
            for (int y = dgc.width-1; y >= 0; y--)
            {
                GUILayout.BeginHorizontal();
                for (int x = 0; x < dgc.height; x++)
                {
                    Room r = dgc.dungeonGenerator.GetRoom(new Vector2(x, y));
                    string render = (r == null) ? "N" : "";
                    GameObject go;
                    if (r != null)
                    {
                        go = dgc.transform.Find($"x{x}y{y}").gameObject;
                        if (go != null)
                        {
                            render = ((ConectionRoom)r).conectedRooms.Count.ToString();
                            if (((ConectionRoom)r).conectedRooms.Count == 4)
                                render = "┼";

                            if (((ConectionRoom)r).start)
                                render = "S";
                            if (((ConectionRoom)r).BossRoom)
                                render = "E";
                            // render = (go.activeSelf) ? "A" : "F";
                        }
                    }
                    if (GUILayout.Button(render))
                    {
                        if (dgc.dungeonGenerator.GetRoom(new Vector2(x, y)) != null)
                        {
                            go = dgc.transform.Find($"x{x}y{y}").gameObject; // GameObject.Find($"x{x}y{y}");
                            if (go != null)
                                go.SetActive(!go.activeSelf);
                        }
                    }
                }
                GUILayout.EndHorizontal();
            }
        }
    }
}
