﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RoomTestManager))]
public class RoomTestManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Export"))
        {
            ((RoomTestManager)target).SaveJson();
        }
        if (GUILayout.Button("Load from JSON"))
        {
            ((RoomTestManager)target).LoadJson();
        }
    }
}
