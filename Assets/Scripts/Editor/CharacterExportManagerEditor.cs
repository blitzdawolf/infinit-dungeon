﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.Scripts.Managers;

[CustomEditor(typeof(CharacterExportManager))]
public class CharacterExportManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CharacterExportManager cem = (CharacterExportManager)target;
        base.OnInspectorGUI();
        EditorGUILayout.LabelField("Loaded Charecters", cem.characters.Count.ToString());
        if (GUILayout.Button("LoadLocations"))
        {
            cem.Load();
        }
        if(GUILayout.Button("Save Json"))
        {
            cem.SaveJson();
        }
    }
}
