﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class DropWindow : EditorWindow
{
    [MenuItem("Oogdin/dropTable")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<DropWindow>("Drop table");
    }

    Vector2 scrolPos;

    private void OnGUI()
    {
        GUILayout.Label("test", EditorStyles.boldLabel);
        scrolPos = GUILayout.BeginScrollView(scrolPos);
        foreach (var obj in Selection.gameObjects)
        {
            if(obj.GetComponent<AICharacter>() != null)
            {
                AICharacter target = obj.GetComponent<AICharacter>();
                GUILayout.Label(obj.name, EditorStyles.boldLabel);
                GUILayout.Label(target.CurrentRoom.ToString(), EditorStyles.boldLabel);
                GUILayout.Label(target.total.ToString());
                foreach (var item in ItemManager.Ins.AllItems)
                {
                    Drop drop = target.dropTable.Where(x => x.id == item.ID).FirstOrDefault();
                    if (drop == null)
                    {
                        if (GUILayout.Button(GenerateTextureFromSprite(ItemManager.Ins.loadSprite(item))) && target.dropTable.Where(x => x.id == item.ID).FirstOrDefault() == null)
                        {
                            target.dropTable.Add(new Drop()
                            {
                                id = item.ID,
                                item = item,
                                dropChange = 0
                            });
                        }
                    }
                    else
                    {
                        GUILayout.BeginVertical();
                        GUILayout.BeginHorizontal();
                        GUILayout.Label($"{item.ID}");
                        drop.dropChange = (int)GUILayout.HorizontalSlider(drop.dropChange, 0, 100);
                        GUILayout.Label($"{(int)drop.dropChange}");
                        if (GUILayout.Button("Remove"))
                        {
                            target.dropTable.Remove(drop);
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.Label($"{item.Name}");
                        GUILayout.Label(GenerateTextureFromSprite(ItemManager.Ins.loadSprite(item)));
                        GUILayout.EndVertical();
                    }
                    // GUILayout.Label($"{item.Name} - {item.ID}");
                }
            }
            else
            {
                GUILayout.Label($"{obj.name}: is not a ai", EditorStyles.boldLabel);
            }
        }
        GUILayout.EndScrollView();
    }

    Texture2D GenerateTextureFromSprite(Sprite aSprite)
    {
        var rect = aSprite.rect;
        var tex = new Texture2D((int)rect.width, (int)rect.height);
        var data = aSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        tex.SetPixels(data);
        tex.Apply(true);
        return tex;
    }
}