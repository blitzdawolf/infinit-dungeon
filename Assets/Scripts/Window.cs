﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    public string WindowName;
    public bool open;

    private void Awake()
    {
        WindowManager.Ins.AddWindow(this);
    }

    public void close()
    {
        gameObject.SetActive(false);
    }
}