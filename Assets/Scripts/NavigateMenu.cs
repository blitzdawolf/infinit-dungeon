﻿using UnityEngine;

[SerializeField]
public class NavigateMenu : MonoBehaviour
{
    public string menuName;
    [HideInInspector]
    public string cameFrom;
    public bool defaultActive;

    private void Start()
    {
        NavigateManager.Ins.AddMenu(this);
    }

    public void Switch(string name)
    {
        NavigateManager.Ins.SwitchMenu(name, this);
    }

    public void GoBack()
    {
        NavigateManager.Ins.SwitchMenu(cameFrom, this);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            GoBack();
        }
    }
}