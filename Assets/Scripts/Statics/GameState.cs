﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public static class GameState
{
    public static readonly string SAVEPATH = Application.streamingAssetsPath + "/../Resources/items";

    public static Dictionary<string, string> checkSums = new Dictionary<string, string>();
    public static bool Development = false;
    public static bool Modded = false;

    public static Dictionary<string, string> LoadCheckSum()
    {
        checkSums = JsonHelper.FromJson(Resources.Load<TextAsset>("items/checkSum").text);
        return checkSums;
    }

    public static void UnLoadCheckSum()
    {
        checkSums = new Dictionary<string, string>();
    }

    public static void CheckSumDictionary(string key, string value)
    {
        if (GameState.checkSums.ContainsKey(key))
            GameState.checkSums[key] = value;
        else
            GameState.checkSums.Add(key, value);
        SaveCheckSum();
    }

    public static void SaveJson(string name, string json)
    {
        string itemMd5 = GameState.MD5Hash(json);
        CheckSumDictionary(name, itemMd5);
        File.WriteAllText(SAVEPATH + $"/{name}.json", json);
    }

    public static void SaveCheckSum()
    {
        File.WriteAllText(SAVEPATH + "/checkSum.json", JsonHelper.ToJson(checkSums));
    }

    public static string MD5Hash(string text)
    {
        MD5 md5 = new MD5CryptoServiceProvider();

        //compute hash from the bytes of text  
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

        //get hash result after compute it  
        byte[] result = md5.Hash;

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            //change it into 2 hexadecimal digits  
            //for each byte  
            strBuilder.Append(result[i].ToString("x2"));
        }

        return strBuilder.ToString();
    }
}
