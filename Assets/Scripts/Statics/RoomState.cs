﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class RoomState
{
    public static readonly int ROOM_SIZE = 22;
    public static int ROOM_SIZE_HALF { get { return ROOM_SIZE / 2; } }
}
