﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testCharacter : MonoBehaviour
{
    public Character character;
    public bool selected = false;
    public float time;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if(time > .5f)
        {
            character.updateMove();
            time = 0;
        }
        transform.position = new Vector3(character.GlobalPosition.x, character.GlobalPosition.y);

        switch (character.FacingDirection)
        {
            case direction.none:
                break;
            case direction.Up:
                gameObject.GetComponentInChildren<SpriteRenderer>().transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case direction.down:
                gameObject.GetComponentInChildren<SpriteRenderer>().transform.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case direction.left:
                gameObject.GetComponentInChildren<SpriteRenderer>().transform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case direction.right:
                gameObject.GetComponentInChildren<SpriteRenderer>().transform.rotation = Quaternion.Euler(0, 0, 270);
                break;
            default:
                break;
        }
    }
}
