﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipPlace
{
    Head,
    Torso,
    Hands,
    Legs,
    Feet,
    Primary,
    Secondary
}
