﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum ItemType
{
    NA = 0,
    Common = 1,
    Uncommon = 10,
    Rare = 20,
    legendary = 50
}
