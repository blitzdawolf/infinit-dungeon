﻿using infdungeon.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Base class for the characters
/// </summary>
[Serializable]
public class Character : MonoBehaviour, ICharacter
{
    #region imported
    /// <summary>
    /// Gets the current room where the cahracter is in (read only)
    /// </summary>
    public Room CurrentRoom
    {
        get
        {
            return currentRoom;
        }
        set
        {
            currentRoom = value;
        }
    }

    public void updateMove()
    {
        if (aStar != null)
        {
            Tile nextTile = aStar.GetNextTile();
            if (nextTile == null)
            {
                aStar = null;
                return;
            }
            direction currentDir = DungeonGenerator.getDerection(new Vector3(nextTile.X, nextTile.Y), new Vector3(CurrentTile.X, CurrentTile.Y));
            facingDirection = currentDir;
            currentTile = nextTile;
            roomPosition = new Vector2Int(currentTile.X, currentTile.Y);
        }
    }

    /// <summary>
    /// Gets the current tile where the chacter is standing on
    /// </summary>
    public Tile CurrentTile => CurrentRoom.getTile(roomPosition.x, roomPosition.y);

    /// <summary>
    /// Characters name
    /// </summary>
    public string Name { get { return name; } }

    /// <summary>
    /// Total heath of the character
    /// </summary>
    public float Health { get => health + CharacterStats.Constitution; }

    /// <summary>
    /// Is the chacrter dead
    /// </summary>
    public bool Dead { get { return dead; } }

    /// <summary>
    /// Is the character Downed
    /// </summary>
    public bool Down { get { return HealthLeft <= 0; } }

    /// <summary>
    /// Health left
    /// </summary>
    public float HealthLeft => Mathf.Clamp(Health - damageTaken, 0, float.MaxValue);

    /// <summary>
    /// TODO: add this logic
    /// </summary>
    public int TurnStamina { get { return turnStamina; } }

    /// <summary>
    /// Characters discription
    /// </summary>
    public string Description => description;

    /// <summary>
    /// The postion in the room
    /// </summary>
    public Vector2Int RoomPosition { get { return roomPosition; } set { roomPosition = value; } }

    /// <summary>
    /// Global position
    /// </summary>
    public Vector2Int GlobalPosition {
        get
        {
            globalPosition = new Vector2Int(currentRoom.X * 23, currentRoom.Y * 23) + RoomPosition;
            return globalPosition;
        }
    }

    /// <summary>
    /// Characters inventory
    /// </summary>
    public IInventory Inventory { get { return inventory; } }

    /// <summary>
    /// To what side is the character facing
    /// </summary>
    public direction FacingDirection => facingDirection;

    /// <summary>
    /// All stats calculated together
    /// </summary>
    public Stats CharacterStats
    {
        get => characterStats + EquipmentInventory.AddingStats();
        set { characterStats = value; }
    }

    /// <summary>
    /// Equipment of the character
    /// </summary>
    public IEquipmentInventory EquipmentInventory => equipmentInventory;
    #endregion

    #region privateFields

    [SerializeField]
    private Room currentRoom;

    protected AStar aStar;

    private Tile currentTile;
    [SerializeField]
    private direction facingDirection;
    [SerializeField]
    private Inventory inventory;
    [SerializeField]
    private new string name;
    [SerializeField]
    private float health;
    [SerializeField]
    private bool dead;
    [SerializeField]
    private bool down;
    [SerializeField]
    private int turnStamina;
    [SerializeField]
    [TextArea(2,25)]
    private string description;
    [SerializeField]
    private Vector2Int roomPosition;
    [SerializeField]
    private Vector2Int globalPosition;
    [SerializeField]
    private Stats characterStats;
    [SerializeField]
    private EquipmentInventory equipmentInventory;
    [SerializeField]
    private float damageTaken;

    public float test;
    #endregion

    public virtual void heal(float amount)
    {
        damageTaken -= amount;
    }

    public bool move(Tile toPosition)
    {
        if(currentRoom != null)
        {
            aStar = new AStar(currentRoom, CurrentTile, toPosition);
            return aStar.ValidPath;
        }
        return false;
    }

    public virtual void takeDamage(float damage)
    {
        damageTaken += damage / (0f + CharacterStats.Defence);
    }

    public void Start()
    {
        equipmentInventory.Legs = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Legs.ID).FirstOrDefault();
        equipmentInventory.Head = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Head.ID).FirstOrDefault();
        equipmentInventory.Hands = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Hands.ID).FirstOrDefault();
        equipmentInventory.Primary = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Primary.ID).FirstOrDefault();
        equipmentInventory.Secondary = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Secondary.ID).FirstOrDefault();
        equipmentInventory.Torso = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Torso.ID).FirstOrDefault();
        equipmentInventory.Feet = (IEquip)ItemManager.Ins.AllItems.Where(item => item.ID == equipmentInventory.Feet.ID).FirstOrDefault();

        name = Guid.NewGuid().ToString();
        gameObject.name = name;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector2(GlobalPosition.x, GlobalPosition.y), (CharacterStats.Speed * GameManager.Ins.GameSpeed) * Time.deltaTime);
        if (aStar != null && Vector2.Distance(transform.position, new Vector2(GlobalPosition.x, GlobalPosition.y)) < 0.25f)
        {
            var next = aStar.GetNextTile();
            if (next == null)
            {
                ChracterManager.Ins.nextTurn(CurrentRoom);
                GetComponent<Animator>().SetBool("Walk", false);
                aStar = null;
            }
            else
            {
                direction dir = DungeonGenerator.getDerection(next.Position, CurrentTile.Position);
                facingDirection = dir;
                int dirs = 0;
                switch (dir)
                {
                    case direction.none:
                        break;
                    case direction.Up:
                        dirs = 2;
                        break;
                    case direction.down:
                        dirs = 0;
                        break;
                    case direction.left:
                        dirs = 1;
                        break;
                    case direction.right:
                        dirs = 3;
                        break;
                    default:
                        break;
                }
                GetComponent<Animator>().SetFloat("Side", dirs);
                GetComponent<Animator>().SetBool("Walk", true);

                if (next.Door)
                {
                    ChracterManager.Ins.nextTurn(CurrentRoom);
                    Door(next);
                }
                else
                {
                    ICharacter v = ChracterManager.Ins.GetCharcacterFromTile(next, currentRoom);
                    if (v != null && v != this)
                    {
                        CharacterInteractions(v, next);
                        ChracterManager.Ins.nextTurn(CurrentRoom);
                        GetComponent<Animator>().SetBool("Walk", false);
                    }
                    else
                    {
                        roomPosition = new Vector2Int(next.X, next.Y);
                    }
                }
            }
        }
        CustomUpdate();
    }

    public virtual void Door(Tile next)
    {

    }

    public virtual void CustomUpdate()
    {

    }

    public virtual void CharacterInteractions(ICharacter character, Tile next)
    {

    }

    public void AnimationDone(String animationName)
    {
        GetComponent<Animator>().SetBool(animationName, false);
    }
}