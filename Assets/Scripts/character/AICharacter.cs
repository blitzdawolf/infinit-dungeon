﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class AICharacter : Character
{
    public List<Drop> dropTable = new List<Drop>();

    public void Start()
    {
        base.Start();
        this.CurrentRoom = GetComponentInParent<RoomController>().Room;
        ChracterManager.Ins.AddChracter(this);
        transform.position = new Vector3(GlobalPosition.x, GlobalPosition.y);

        if (CurrentTile.wall)
        {
            RoomPosition = new Vector2Int(Random.Range(1, 21), Random.Range(1, 21));
        }
    }

    public void setRoom(Room room)
    {
        CurrentRoom = room;
    }

    public float total
    {
        get
        {
            float flt = 0;
            foreach (var item in dropTable)
            {
                flt += item.dropChange;
            }
            return flt;
        }
    }

    public override void CustomUpdate()
    {
        test = this.HealthLeft;
    }

    public override void CharacterInteractions(ICharacter character, infdungeon.objects.Tile next)
    {
        if (character.GetType() == typeof(PlayerCharacter) && !character.Down)
        {
            GetComponent<Animator>().SetBool("Walk", false);
            GetComponent<Animator>().SetBool("Slash", true);
            character.takeDamage(0f + this.CharacterStats.Attack);
            aStar = null;
        }
        else
        {
            RoomPosition = new Vector2Int(next.X, next.Y);
            base.CharacterInteractions(character, next);
        }
    }
}