﻿using System;

/// <summary>
/// Basic stat
/// </summary>
[Serializable]
public class Stat
{
    public int amount;
    public float multiplier;

    public static Stat operator *(Stat stat1, Stat stat2) => new Stat() { amount = stat1.amount * stat2.amount, multiplier = stat1.multiplier * stat2.multiplier };
    public static Stat operator *(Stat stat1, int amt) => new Stat() { amount = stat1.amount * amt, multiplier = stat1.multiplier * amt };

    public static Stat operator +(Stat stat1, Stat stat2) => new Stat() {
        amount = 1,// stat1.amount + stat2.amount
        multiplier = (stat1.multiplier * stat1.amount) + (stat2.multiplier * stat2.amount)
    };
    public static Stat operator +(Stat stat1, int amt) => new Stat() { amount = stat1.amount + amt, multiplier = stat1.multiplier + amt };

    public static Stat operator -(Stat stat1, Stat stat2) => new Stat() { amount = stat1.amount - stat2.amount, multiplier = stat1.multiplier - stat2.multiplier };
    public static Stat operator -(Stat stat1, int amt) => new Stat() { amount = stat1.amount - amt, multiplier = stat1.multiplier - amt };

    public static float operator *(float flt, Stat stat)
    {
        return flt * (stat.amount * stat.multiplier);
    }
    public static float operator *(Stat stat, float flt)
    {
        return flt * stat;
    }
    public static float operator +(float flt, Stat stat)
    {
        return flt + (stat.amount * stat.multiplier);
    }

    public override string ToString()
    {
        return $"Ammount: {amount}, Multipier: {multiplier}, Total: {amount * multiplier}";
    }
}