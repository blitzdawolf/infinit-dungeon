﻿using infdungeon.objects;
using UnityEngine;

/// <summary>
/// Characters interface
/// So we can train the AI without unity
/// </summary>
public interface ICharacter
{
    string Name { get; }
    float Health { get; }
    bool Dead { get; }
    bool Down { get; }
    float HealthLeft { get; }
    int TurnStamina { get; }
    Room CurrentRoom { get; }
    IInventory Inventory { get; }
    direction FacingDirection { get; }
    IEquipmentInventory EquipmentInventory { get; }
    Stats CharacterStats { get; }
    Tile CurrentTile { get; }

    string Description { get; }

    Vector2Int RoomPosition { get; }
    Vector2Int GlobalPosition { get; }

    bool move(Tile toPosition);
    void takeDamage(float damage);
    void heal(float amount);
    void CustomUpdate();
}
