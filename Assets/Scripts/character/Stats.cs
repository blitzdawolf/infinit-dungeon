﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Stats for the character and items
/// </summary>
[Serializable]
public class Stats
{
    public Stat Attack;
    public Stat Defence;
    public Stat Speed;
    public Stat Constitution;

    public Stats()
    {
        Attack = new Stat();
        Defence = new Stat();
        Speed = new Stat();
        Constitution = new Stat();
    }

    public static Stats operator *(Stats stat1, Stats stat2)
    {
        return new Stats()
        {
            Attack = stat1.Attack * stat2.Attack,
            Defence = stat1.Defence * stat2.Defence,
            Speed = stat1.Speed * stat2.Speed,
            Constitution = stat1.Constitution * stat2.Constitution
        };
    }
    public static Stats operator *(Stats stats, int amt)
    {
        return stats;
    }

    public static Stats operator +(Stats stat1, Stats stat2)
    {
        return new Stats()
        {
            Attack = stat1.Attack + stat2.Attack,
            Defence = stat1.Defence + stat2.Defence,
            Speed = stat1.Speed + stat2.Speed,
            Constitution = stat1.Constitution + stat2.Constitution
        };
    }
    /*public static Stats operator +(Stats stats, int amt)
    {
        return stats;
    }*/

    public static Stats operator -(Stats stat1, Stats stat2)
    {
        return new Stats()
        {
            Attack = stat1.Attack - stat2.Attack,
            Defence = stat1.Defence - stat2.Defence,
            Speed = stat1.Speed - stat2.Speed,
            Constitution = stat1.Constitution - stat2.Constitution
        };
    }
    public static Stats operator -(Stats stats, int amt)
    {
        return stats;
    }

    public override string ToString()
    {
        string str = $@"
Atack: {Attack}
Defence: {Defence}
Speed: {Speed}
Constitution: {Constitution}
";

        return str;
    }
}
