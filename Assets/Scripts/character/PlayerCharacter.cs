﻿using System;
using infdungeon.objects;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class PlayerCharacter : Character
{
    public Color debugColor = new Color(0, 0, 0, 255);

    public void Start()
    {
        base.Start();
        GetComponentInChildren<SpriteRenderer>().color = debugColor;

        if (CurrentTile.wall)
        {
            RoomPosition = new Vector2Int(Random.Range(1, 21), Random.Range(1, 21));
        }

        transform.position = new Vector3(GlobalPosition.x, GlobalPosition.y);
    }

    public override void CustomUpdate()
    {
        test = this.HealthLeft;
    }

    public override void CharacterInteractions(ICharacter character, infdungeon.objects.Tile next)
    {
        if (character.GetType() == typeof(AICharacter) && !character.Down)
        {
            GetComponent<Animator>().SetBool("Walk", false);
            GetComponent<Animator>().SetBool("Slash", true);
            character.takeDamage(0f + this.CharacterStats.Attack);
            aStar = null;
        }
        else if(character.GetType() == typeof(PlayerCharacter) && character.Down)
        {
            character.heal(character.Health / 2);
        }
        else
        {
            // RoomPosition = new Vector2Int(next.X, next.Y);
            base.CharacterInteractions(character, next);
        }
        aStar = null;
    }

    public override void Door(Tile next)
    {
        DungeonGeneratorController dgc = GameObject.Find("DungeonGenerator").GetComponent<DungeonGeneratorController>();
        RoomController rc = dgc.map[next.nextRoom.X, next.nextRoom.Y].GetComponent<RoomController>();

        Room lastRoom = CurrentRoom;
        RoomController lastRc = dgc.map[lastRoom.X, lastRoom.Y].GetComponent<RoomController>();

        var t = lastRc.charactersHolder.GetComponentsInChildren<PlayerCharacter>();

        if (t.Length <= 1)
        {
            lastRc.started = false;
            Camera.main.GetComponent<CameraMovement>().currentRoom = rc.gameObject;
            rc.gameObject.SetActive(true);
            lastRc.gameObject.SetActive(false);
        }

        if (rc == null)
        {
            return;
        }

        RoomPosition = new Vector2Int(next.X, next.Y);

        if(RoomPosition.x == 0)
        {
            RoomPosition = new Vector2Int(RoomState.ROOM_SIZE-1, RoomState.ROOM_SIZE_HALF);
        }
        if(RoomPosition.y == 0)
        {
            RoomPosition = new Vector2Int(RoomState.ROOM_SIZE_HALF, RoomState.ROOM_SIZE-1);
        }

        if (RoomPosition.x == RoomState.ROOM_SIZE)
        {
            RoomPosition = new Vector2Int(1, RoomState.ROOM_SIZE_HALF);
        }
        if (RoomPosition.y == RoomState.ROOM_SIZE)
        {
            RoomPosition = new Vector2Int(RoomState.ROOM_SIZE_HALF, 1);
        }

        this.CurrentRoom = rc.Room;
        ChracterManager.Ins.UpdateCharacter(this, lastRoom, CurrentRoom);
        transform.position = new Vector3(GlobalPosition.x, GlobalPosition.y);
        transform.SetParent(rc.transform.Find("charactersHolder"));
    }
}