﻿using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterSelection : MonoBehaviour
{
    public List<player> myCharacters = new List<player>();
    public static CharacterSelection Ins;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        Ins = this;
    }

    public void AddCharacter(PlayerCharacter c)
    {
        player p = new player()
        {
            PlayerCharacter = c,
            addedStat = new Stats()
        };
        myCharacters.Add(p);
    }

    public void RemoveCharacter(PlayerCharacter c)
    {
        myCharacters.Remove(myCharacters.Where(item => item.PlayerCharacter == c).FirstOrDefault());
    }
}
