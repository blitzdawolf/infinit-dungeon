﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterDetal : MonoBehaviour
{
    public Text Name;
    public Text Description;
    public Image image;
    public PlayerCharacter character;

    void Start()
    {
        Name.text = $"Name: {character.gameObject.name}";
        Description.text = $@"Stats:{character.CharacterStats}
Description: {character.Description}";
        image.sprite = character.GetComponentInChildren<SpriteRenderer>().sprite;
        image.color = character.debugColor;
    }

    void Update()
    {
        
    }
}
