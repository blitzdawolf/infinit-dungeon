﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectorMaker : MonoBehaviour
{
    public GameObject parent;
    public GameObject prefab;

    public GameObject DetailParrent;
    public GameObject DetailPrefab;

    // Start is called before the first frame update
    void Start()
    {
        List<PlayerCharacter> t = ChracterManager.GetAllPlayerCharacter();
        foreach (var item in t)
        {
            GameObject obj = Instantiate(prefab);
            obj.GetComponent<CharacterSelector>().character = item;
            obj.transform.SetParent(parent.transform);

            GameObject dObj = Instantiate(DetailPrefab);
            dObj.GetComponent<CharacterDetal>().character = item;
            dObj.transform.SetParent(DetailParrent.transform);
        }
    }
}
