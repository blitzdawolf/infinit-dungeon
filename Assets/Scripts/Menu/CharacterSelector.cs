﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelector : MonoBehaviour
{
    public bool Selected = false;
    public Image image;
    public TextMeshProUGUI nameText;
    public Toggle Toggle;
    public PlayerCharacter character;

    private void Start()
    {
        image.sprite = character.GetComponentInChildren<SpriteRenderer>().sprite;
        image.color = character.debugColor;
        nameText.text = character.gameObject.name;
    }

    public void OnSectChange()
    {
        Selected = !Selected;
        Toggle.isOn = Selected;
        if (Selected)
        {
            CharacterSelection.Ins.AddCharacter(character);
        }
        else
        {
            CharacterSelection.Ins.RemoveCharacter(character);
        }
    }
}
