﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.PathFinding
{
    public class PathEdge<T>
    {
        public float cost;

        public PathNode<T> node;
    }
}
