﻿using Assets.Scripts.PathFinding;
using infdungeon.objects;
using Priority_Queue;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AStar
{
    Queue<Tile> path;
    private bool validPath = false;

    public bool ValidPath
    {
        get { return validPath; }
    }

    public int PathLength { get {
            if (path == null)
                return int.MaxValue;
            return path.Count;
        } }

    public AStar(Room room, Tile tileStart, Tile tileEnd)
    {
        if(room.tileGraph == null || tileStart == null || tileEnd == null)
        {
            if(tileStart == null)
            {
                Debug.LogError("Astar: Start is null");
            }
            if(room.tileGraph == null)
            {
                Debug.LogError("Astar: tileGraph is null");
            }
            // TODO: Add error handelingc
            return;
        }

        Dictionary<Tile, PathNode<Tile>> nodes = room.tileGraph.nodes;

        if (nodes.ContainsKey(tileStart) == false)
        {
            // TODO: Add error handelingc
            return;
        }
        if (nodes.ContainsKey(tileEnd) == false)
        {
            // TODO: Add error handelingc
            return;
        }

        PathNode<Tile> start = nodes[tileStart];
        PathNode<Tile> end = nodes[tileEnd];

        List<PathNode<Tile>> ClosedSet = new List<PathNode<Tile>>();

        SimplePriorityQueue<PathNode<Tile>> OpenSet = new SimplePriorityQueue<PathNode<Tile>>();
        OpenSet.Enqueue(start, 0);

        Dictionary<PathNode<Tile>, PathNode<Tile>> Came_From = new Dictionary<PathNode<Tile>, PathNode<Tile>>();

        Dictionary<PathNode<Tile>, float> g_score = new Dictionary<PathNode<Tile>, float>();
        foreach (PathNode<Tile> n in nodes.Values)
        {
            g_score[n] = Mathf.Infinity;
        }
        g_score[nodes[tileStart]] = 0;

        Dictionary<PathNode<Tile>, float> f_score = new Dictionary<PathNode<Tile>, float>();
        foreach (PathNode<Tile> n in nodes.Values)
        {
            f_score[n] = Mathf.Infinity;
        }
        f_score[nodes[tileStart]] = heuristic_cost_estimate(start, end);

        while(OpenSet.Count > 0)
        {
            PathNode<Tile> current = OpenSet.Dequeue();

            if(current == end)
            {
                reconstruct_path(Came_From, current);
                return;
            }
            ClosedSet.Add(current);

            foreach (PathEdge<Tile> pathEdge in current.edges)
            {
                PathNode<Tile> neighbor = pathEdge.node;
                if( ClosedSet.Contains(neighbor))
                    continue;

                float tentative_g_score = g_score[current] + dist_between(current, neighbor);

                if (OpenSet.Contains(neighbor) && tentative_g_score >= g_score[neighbor])
                    continue;

                Came_From[neighbor] = current;
                g_score[neighbor] = tentative_g_score;
                f_score[neighbor] = g_score[neighbor] + heuristic_cost_estimate(neighbor, end);

                if (OpenSet.Contains(neighbor) == false)
                {
                    OpenSet.Enqueue(neighbor, f_score[neighbor]);
                }
            }
        }
    }

    float dist_between(PathNode<Tile> a, PathNode<Tile> b)
    {
        // We can make assumptions because we know we're working
        // on a grid at this point.

        // Hori/Vert neighbours have a distance of 1
        if (Mathf.Abs(a.data.X - b.data.X) + Mathf.Abs(a.data.Y - b.data.Y) == 1)
        {
            return 1f;
        }

        // Diag neighbours have a distance of 1.41421356237	
        if (Mathf.Abs(a.data.X - b.data.X) == 1 && Mathf.Abs(a.data.Y - b.data.Y) == 1)
        {
            return 1.41421356237f;
        }

        // Otherwise, do the actual math.
        return Mathf.Sqrt(
            Mathf.Pow(a.data.X - b.data.X, 2) +
            Mathf.Pow(a.data.Y - b.data.Y, 2)
        );

    }

    float heuristic_cost_estimate(PathNode<Tile> start, PathNode<Tile> goal)
    {
        return Mathf.Sqrt(
            Mathf.Pow(start.data.X - goal.data.X, 2) +
            Mathf.Pow(start.data.Y - goal.data.Y, 2));
    }

    void reconstruct_path(Dictionary<PathNode<Tile>, PathNode<Tile>> Came_From, PathNode<Tile> current)
    {
        Queue<Tile> total_path = new Queue<Tile>();
        total_path.Enqueue(current.data);
        while (Came_From.ContainsKey(current))
        {
            current = Came_From[current];
            total_path.Enqueue(current.data);
        }

        validPath = true;
        path = new Queue<Tile>(total_path.Reverse());
    }

    public Tile GetNextTile()
    {
        if (path == null)
            return null;
        if (path.Count > 0)
            return path.Dequeue();
        return null;
    }
}
