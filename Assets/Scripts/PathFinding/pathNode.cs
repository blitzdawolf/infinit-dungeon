﻿using infdungeon.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.PathFinding
{
    public class PathNode<T>
    {
        public T data;

        public PathEdge<T>[] edges;
    }
}
