﻿using infdungeon.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PathFinding
{
    public class TileGraph
    {
        public Dictionary<Tile, PathNode<Tile>> nodes;

        public TileGraph(Room room)
        {
            // Debug.Log("Starting to create graph");
            nodes = new Dictionary<Tile, PathNode<Tile>>();

            for (int x = 0; x < room.tiles.GetLength(0); x++)
            {
                for (int y = 0; y < room.tiles.GetLength(1); y++)
                {
                    Tile tile = room.tiles[x, y];
                    if(tile.cost > 0)
                    {
                        PathNode<Tile> node = new PathNode<Tile>();
                        node.data = tile;
                        nodes.Add(tile, node);
                    }
                }
            }

            foreach (Tile tile in nodes.Keys)
            {
                PathNode<Tile> n = nodes[tile];

                List<PathEdge<Tile>> edges = new List<PathEdge<Tile>>();

                Tile[] neigbours = tile.GetNeighbours(true);

                for (int i = 0; i < neigbours.Length; i++)
                {
                    if (neigbours[i] != null && neigbours[i].cost > 0)
                    {
                        PathEdge<Tile> e = new PathEdge<Tile>();
                        e.cost = neigbours[i].cost;
                        e.node = nodes[neigbours[i]];
                        edges.Add(e);
                    }
                }

                n.edges = edges.ToArray();
            }
        }
    }
}
