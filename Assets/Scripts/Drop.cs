﻿using UnityEngine;
using System;

[Serializable]
public class Drop
{
    public string id;
    public IItem item;
    public float dropChange;
}