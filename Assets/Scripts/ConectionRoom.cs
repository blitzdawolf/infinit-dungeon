﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConectionRoom : Room
{
    public List<Vector2> saveConectedRooms = new List<Vector2>();
    [System.NonSerialized]
    public List<Room> conectedRooms = new List<Room>();

    public bool start;

    public ConectionRoom()
    {

    }

    public ConectionRoom(Room r)
    {
        base.Name = r.Name;
        base.BossRoom = r.BossRoom;
        base.EndLevel = r.EndLevel;
        base.Location = r.Location;
        base.QuestRoom = r.QuestRoom;
        base.SideRoom = r.SideRoom;
        base.StartLevel = r.StartLevel;
    }

    public ConectionRoom(Room r, int x, int y) : this(r)
    {
        this.x = x;
        this.y = y;
    }
}
