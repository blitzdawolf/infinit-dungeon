﻿using Assets.Scripts.PathFinding;
using UnityEngine;
using UnityEngine.Tilemaps;
using Tile = infdungeon.objects.Tile;

public class TuturialRoomCreator : RoomController
{
    [HideInInspector]
    public Room myRoom;

    private void Awake()
    {
        myRoom = new Room();

        Tilemap wall = transform.Find("Wall").GetComponent<Tilemap>();
        int width = wall.size.x;
        int heigth = wall.size.y;

        int bigestWidth = 0;
        int bigestheigth = 0;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < heigth; y++)
            {
                TerrainTile tb = (TerrainTile)wall.GetTile(wall.WorldToCell((new Vector3(x, y))));
                if(tb != null)
                {
                    if (x > bigestWidth)
                        bigestWidth = x;
                    if (y > bigestheigth)
                        bigestheigth = y;
                }
            }
        }

        this.Room = myRoom;

        bigestheigth++;
        bigestWidth++;

        myRoom.tiles = new Tile[bigestWidth, bigestheigth];

        for (int x = 0; x < bigestWidth; x++)
        {
            for (int y = 0; y < bigestheigth; y++)
            {
                myRoom.tiles[x, y] = new Tile(x, y);
                myRoom.tiles[x, y].room = myRoom;
                TerrainTile tb = (TerrainTile)wall.GetTile(wall.WorldToCell((new Vector3(x, y))));

                if (tb != null)
                {
                    myRoom.tiles[x, y].wall = true;
                }
            }
        }

        FindObjectOfType<CameraMovement>().room = myRoom;

        myRoom.tileGraph = new TileGraph(myRoom);

        Debug.Log(wall.size);
    }
}