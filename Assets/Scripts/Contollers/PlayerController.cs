﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private void Awake()
    {
        ChracterManager.Ins.RegisterPlayerTurn(Turn);
    }

    private void Turn(PlayerCharacter character)
    {
        // Debug.Log("Player has new turn", character);
        if (!character.Down)
            Camera.main.GetComponent<CameraMovement>().currentControlCharacter = character;
        else
            ChracterManager.Ins.nextTurn(character.CurrentRoom);
    }
}
