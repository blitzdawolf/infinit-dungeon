﻿using System.Collections;
using UnityEngine;

public class AIController : MonoBehaviour
{
    private void Awake()
    {
        ChracterManager.Ins.RegisterAITurn(Turn);
    }

    private IEnumerator SetTurn(AICharacter character)
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 1f));
        if (character.Down || character.Dead)
        {
            Debug.Log("AI is dead");
            ChracterManager.Ins.nextTurn(character.CurrentRoom);
        }
        else
        {
            Vector2Int movement = new Vector2Int(Random.Range(-2, 3), Random.Range(-2, 3));// new Vector2Int(Random.Range(-1, 2), Random.Range(-1, 2));

            System.Tuple<PlayerCharacter, int> t = ChracterManager.Ins.getClosestPlayer(character.CurrentTile, character.CurrentRoom);
            if (t.Item2 < 3)
            {
                character.move(t.Item1.CurrentTile);
            }
            else
            {
                character.move(character.CurrentRoom.getTile(character.CurrentTile.X + movement.x, character.CurrentTile.Y + movement.y));
            }
        }
    }

    private void Turn(AICharacter character)
    {
        StartCoroutine(SetTurn(character));
    }
}
