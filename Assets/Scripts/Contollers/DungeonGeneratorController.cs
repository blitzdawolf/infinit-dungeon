﻿using Assets.Scripts.PathFinding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;


public class DungeonGeneratorController : MonoBehaviour
{
    private GameObject lastLoaded;
    public List<player> myCharacters = new List<player>();

    public GameObject[,] map;

    [Range(5, 50)]
    public int width = 5;
    [Range(5, 50)]
    public int height = 5;
    public DungeonGenerator dungeonGenerator;
    
    public Tile door;

    private void Start()
    {
        if(CharacterSelection.Ins != null)
        {
            myCharacters = CharacterSelection.Ins.myCharacters;
        }

        map = new GameObject[width, height];
        dungeonGenerator = new DungeonGenerator(width, height);
        dungeonGenerator.GenerateDungeon(1, CreateRoom);
        dungeonGenerator.SaveJson();
    }

    void CreateRoom(ConectionRoom room)
    {
        GameObject roomParent = new GameObject();
        GameObject charactersHolder = new GameObject();
        GameObject roomHolder = new GameObject();

        charactersHolder.transform.parent = roomParent.transform;
        charactersHolder.name = "charactersHolder";
        roomHolder.transform.parent = roomParent.transform;
        roomHolder.name = "roomHolder";
        roomParent.transform.parent = transform;
        roomParent.name = $"{room.X}:{room.Y}";

        map[room.X, room.Y] = roomParent;

        GameObject go = Resources.Load<GameObject>($"Rooms/{room.Location}");
        GameObject goRoom = GameObject.Instantiate(go);

        goRoom.name = $"{room.X}:{room.Y}";
        goRoom.transform.SetParent(roomHolder.transform);

        Grid g = goRoom.GetComponent<Grid>();
        Tilemap[] maps = g.GetComponentsInChildren<Tilemap>();
        Tilemap wall = maps.Where(x => x.name == "Wall").FirstOrDefault();
        Tilemap doors = maps.Where(x => x.name == "Door").FirstOrDefault();

        roomParent.transform.position = new Vector3(wall.size.x * room.X, wall.size.y * room.Y);

        room.tiles = new infdungeon.objects.Tile[RoomState.ROOM_SIZE+1, RoomState.ROOM_SIZE+1];

        ConectionRoom[] test = new ConectionRoom[4];

        DrawCube(new Vector3(room.X, room.Y));

        if (room.conectedRooms.Count > 0)
        {
            ConectionRoom nbRoom = (ConectionRoom)room.conectedRooms[0];
            foreach (ConectionRoom item in room.conectedRooms)
            {
                Debug.DrawLine(new Vector3(room.X, room.Y) + Vector3.one * .5f, new Vector3(item.X, item.Y) + Vector3.one * .5f, Color.green, 500000000000);
                direction direction = direction.none;
                if (item.X > room.X)
                {
                    direction = direction.right;
                }
                if (item.X < room.X)
                {
                    direction = direction.left;
                }

                if (item.Y > room.Y)
                {
                    direction = direction.Up;
                }
                if (item.Y < room.Y)
                {
                    direction = direction.down;
                }

                switch (direction)
                {
                    case direction.Up:
                        // North
                        test[0] = item;
                        doors.SetTile(new Vector3Int(RoomState.ROOM_SIZE_HALF, RoomState.ROOM_SIZE, 0), door);
                        wall.SetTile(new Vector3Int(RoomState.ROOM_SIZE_HALF, RoomState.ROOM_SIZE, 0), null);
                        Tile tb = (Tile)doors.GetTile(wall.WorldToCell((new Vector3(RoomState.ROOM_SIZE_HALF, RoomState.ROOM_SIZE)) + goRoom.transform.position));
                        tb.name = "north";
                        break;
                    case direction.down:
                        // South
                        test[2] = item;
                        doors.SetTile(new Vector3Int(RoomState.ROOM_SIZE_HALF, 0, 0), door);
                        wall.SetTile(new Vector3Int(RoomState.ROOM_SIZE_HALF, 0, 0), null);
                        break;
                    case direction.left:
                        // West
                        test[3] = item;
                        doors.SetTile(new Vector3Int(0, RoomState.ROOM_SIZE_HALF, 0), door);
                        wall.SetTile(new Vector3Int(0, RoomState.ROOM_SIZE_HALF, 0), null);
                        break;
                    case direction.right:
                        // East
                        test[1] = item;
                        doors.SetTile(new Vector3Int(RoomState.ROOM_SIZE, RoomState.ROOM_SIZE_HALF, 0), door);
                        wall.SetTile(new Vector3Int(RoomState.ROOM_SIZE, RoomState.ROOM_SIZE_HALF, 0), null);
                        break;
                    default:
                        break;
                }
            }
        }

        for (int x = 0; x <= RoomState.ROOM_SIZE; x++)
        {
            for (int y = 0; y <= RoomState.ROOM_SIZE; y++)
            {
                room.tiles[x, y] = new infdungeon.objects.Tile();
                Tile tb = (Tile)wall.GetTile(wall.WorldToCell((new Vector3(x, y)) + goRoom.transform.position));
                Tile Door = (Tile)doors.GetTile(doors.WorldToCell((new Vector3(x, y)) + goRoom.transform.position));
                if (tb != null)
                {
                    room.tiles[x, y].wall = true;
                }
                if(Door != null)
                {
                    room.tiles[x, y].Door = true;
                    room.tiles[x, y].wall = false;
                    if (x == 0)
                    {
                        room.tiles[x,y].nextRoom = test[3];
                    }
                    if (y == 0)
                    {
                        room.tiles[x, y].nextRoom = test[2];
                    }
                    if (x == RoomState.ROOM_SIZE)
                    {
                        room.tiles[x, y].nextRoom = test[1];
                    }
                    if (y == RoomState.ROOM_SIZE)
                    {
                        room.tiles[x, y].nextRoom = test[0];
                    }
                }
                room.tiles[x, y].room = room;
                room.tiles[x, y].X = x;
                room.tiles[x, y].Y = y;
            }
        }

        roomParent.AddComponent<RoomController>().Room = room;
        room.tileGraph = new TileGraph(room);

        if (!room.start)
        {
            roomParent.SetActive(false);
        }
        else
        {
            Camera.main.transform.position = new Vector3(goRoom.transform.position.x + RoomState.ROOM_SIZE_HALF, goRoom.transform.position.y + RoomState.ROOM_SIZE_HALF, -10);
            Camera.main.GetComponent<CameraMovement>().currentRoom = goRoom;
            Camera.main.GetComponent<CameraMovement>().room = room;

            // var v = GameObject.FindObjectsOfType<PlayerCharacter>();
            foreach (var p in myCharacters)
            {
                PlayerCharacter item = p.CompletePlayerCharacter();
                item.transform.SetParent(charactersHolder.transform);

                item.CurrentRoom = room;
                item.RoomPosition = new Vector2Int(Random.Range(1, 21), Random.Range(1, 21));
                item.transform.position = new Vector3(item.GlobalPosition.x, item.GlobalPosition.y);
                if (!ChracterManager.Ins.AddChracter(item))
                {
                    Debug.Log("Oops something went wrong");
                }
            }
        }
    }

    public void DrawCube(Vector3 pos)
    {
        Debug.DrawLine(
            new Vector3(.25f, .25f) + pos,
            new Vector3(.25f, .75f) + pos
            , Color.red, 500000000000);

        Debug.DrawLine(
            new Vector3(.25f, .75f) + pos,
            new Vector3(.75f, .75f) + pos
            , Color.red, 500000000000);

        Debug.DrawLine(
            new Vector3(.75f, .25f) + pos,
            new Vector3(.75f, .75f) + pos
            , Color.red, 500000000000);

        Debug.DrawLine(
            new Vector3(.25f, .25f) + pos,
            new Vector3(.75f, .25f) + pos
            , Color.red, 500000000000);
    }
}

[System.Serializable]
public struct player
{
    public PlayerCharacter PlayerCharacter;
    public Stats addedStat;

    public PlayerCharacter CompletePlayerCharacter()
    {
        PlayerCharacter pc = GameObject.Instantiate(PlayerCharacter).GetComponent<PlayerCharacter>();

        pc.CharacterStats += addedStat;
        return pc;
    }
}