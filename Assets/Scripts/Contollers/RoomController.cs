﻿using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    public Room Room;
    public bool started = false;
    public GameObject charactersHolder;

    public void Start()
    {
        charactersHolder = transform.Find("charactersHolder").gameObject;
        // AddAi();
        for (int i = 0; i < Random.Range(1,3); i++)
        {
            AddAi();
        }

        GameObject.Find("UI").GetComponent<QueueUI>().currentRoom = Room;
    }

    private void OnEnable()
    {
        
    }

    private void LateUpdate()
    {
        if (ChracterManager.Ins.GetCount(Room) >= transform.Find("charactersHolder").childCount && !started)
        {
            started = true;
            ChracterManager.Ins.CreateQueue(Room);
            ChracterManager.Ins.nextTurn(Room);
        }
    }

    public void AddAi()
    {
        charactersHolder = transform.Find("charactersHolder").gameObject;

        List<AICharacter> aicharacters = ChracterManager.Ins.GetAiCharacters();
        AICharacter aiChar = aicharacters[Random.Range(0, aicharacters.Count)];
        GameObject obj = GameObject.Instantiate(aiChar.gameObject);
        aiChar.RoomPosition = new Vector2Int(Random.Range(1, 21), Random.Range(1, 21));
        aiChar.name = $"{Room.X}, {Room.Y}";
        obj.transform.name = $"Room {aiChar.CurrentRoom.X}, {aiChar.CurrentRoom.Y}";

        obj.transform.position = new Vector3(aiChar.GlobalPosition.x, aiChar.GlobalPosition.y);
        obj.transform.parent = charactersHolder.transform;
    }
}