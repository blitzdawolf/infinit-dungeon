﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraMovement : MonoBehaviour
{
    public float speed = 5f;
    [Range(5,30)]
    public float goBackSpeed;
    public GameObject currentRoom;
    public DungeonGeneratorController ddc;

    public PlayerCharacter currentControlCharacter;

    public Room room;

    public GameObject selector;
    public GameObject active;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        if (currentRoom == null)
            return;

        if (currentControlCharacter != null)
        {
            active.SetActive(true);
            Vector2Int v2i = currentControlCharacter.GlobalPosition;
            active.transform.position = new Vector3(v2i.x+.5f, v2i.y+.5f);
        }
        else
        {
            active.SetActive(false);
        }

        Vector3 movement = (new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * speed);
        movement *= Camera.main.orthographicSize;
        gameObject.transform.position += movement * Time.deltaTime;

        Camera.main.orthographicSize -= Input.GetAxis("Mouse ScrollWheel");

        float xCorrection = 0;
        float yCorrection = 0;

        if (transform.position.x > currentRoom.transform.position.x + room.Width)
        {
            xCorrection = transform.position.x - (currentRoom.transform.position.x + RoomState.ROOM_SIZE);
        }
        if(transform.position.x < currentRoom.transform.position.x)
        {
            xCorrection = transform.position.x - currentRoom.transform.position.x;
        }

        if (transform.position.y > currentRoom.transform.position.y + room.Height)
        {
            yCorrection = transform.position.y - (currentRoom.transform.position.y + RoomState.ROOM_SIZE);
        }
        if(transform.position.y < currentRoom.transform.position.y)
        {
            yCorrection = transform.position.y - currentRoom.transform.position.y;
        }

        transform.position -= new Vector3(xCorrection, yCorrection) * (goBackSpeed * Time.deltaTime);

        Vector3 v3 = Input.mousePosition;
        v3 = Camera.main.ScreenToWorldPoint(v3);

        v3 -= currentRoom.transform.position;
        v3 = new Vector3((int)v3.x, (int)v3.y);
        Vector3 testPosition = new Vector3((int)v3.x + .5f, (int)v3.y + .5f);
        selector.transform.position = testPosition + currentRoom.transform.position;

        if (Input.GetMouseButtonDown(0))
        {
            if(currentControlCharacter != null)
            {
                if (currentControlCharacter.move(currentControlCharacter.CurrentRoom.getTile((int)v3.x, (int)v3.y)))
                {
                    currentControlCharacter = null;
                }
            }
        }
    }

    private void OnGUI()
    {
        #if UNITY_EDITOR
        GUI.Label(new Rect(10, 10, Screen.width, 50), $"goBackSpeed: {goBackSpeed}");

        if (currentRoom == null)
            return;

        Vector3 v3 = Input.mousePosition;
        v3 = Camera.main.ScreenToWorldPoint(v3);

        v3 -= currentRoom.transform.position;

        GUI.Label(new Rect(10, 30, Screen.width, 50), $"mousePosition: ({(int)v3.x}, {(int)v3.y})");
        #endif
    }
}
