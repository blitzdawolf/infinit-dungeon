﻿using System;
using System.Collections.Generic;

public static class ListHelper<T>
{
    public static void ShiftLeft(ref List<T> source)
    {
        List<T> copy = new List<T>();
        copy.Add(source[source.Count - 1]);
        for (int i = 0; i < source.Count - 1; i++)
        {
            copy.Add(source[i]);
        }
        source = copy;
        // return copy;
    }

    public static void ShiftRight(ref List<T> source)
    {
        List<T> copy = new List<T>();
        for (int i = 1; i < source.Count; i++)
        {
            copy.Add(source[i]);
        }
        copy.Add(source[0]);
        source = copy;
    }

    private static Random rng = new Random();

    public static void Shuffle(ref List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
