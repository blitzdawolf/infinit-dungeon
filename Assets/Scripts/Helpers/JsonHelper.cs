﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    public static string ToJson(Dictionary<string, string> dic)
    {
        string str = "{";
        dic.Keys.ToList().ForEach(x =>
        {
            str += $"\"{x}\":\"{dic[x]}\",";
        });
        str = str.Remove(str.Length - 1);
        str += "}";
        return str;
    }

    public static Dictionary<string, string> FromJson(string json)
    {
        Dictionary<string, string> strings = new Dictionary<string, string>();
        string str = json;
        str = str.Replace('"', ' ').Replace('{', ' ').Replace('}', ' ');
        str.Split(new char[1] { ',' }).ToList().ForEach(x =>
        {
            string[] keyValue = x.Split(new char[1] { ':' });
            strings.Add(keyValue[0].Trim(), keyValue[1].Trim());
        });
        return strings;
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}