﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DungeonGenerator
{
    public static readonly int MIN_WALL_DISTANCE = 0;

    public int Width => width;
    public int Height => height;

    private int width, height;
    private ConectionRoom[,] rooms;

    public DungeonGenerator(int width = 5, int height = 5)
    {
        this.width = width;
        this.height = height;
        rooms = new ConectionRoom[width, height];
    }

    public void GenerateDungeon(int level, System.Action<ConectionRoom> t)
    {
        Vector2 start = new Vector2(Random.Range(MIN_WALL_DISTANCE, width - MIN_WALL_DISTANCE), Random.Range(MIN_WALL_DISTANCE, height - MIN_WALL_DISTANCE));
        Vector2 end = new Vector2(Random.Range(MIN_WALL_DISTANCE, width - MIN_WALL_DISTANCE), Random.Range(MIN_WALL_DISTANCE, height - MIN_WALL_DISTANCE));
        while (Vector2.Distance(start, end) < ((width >= height) ? width : height) / 2)
        {
            start = new Vector2(Random.Range(MIN_WALL_DISTANCE, width - MIN_WALL_DISTANCE), Random.Range(MIN_WALL_DISTANCE, height - MIN_WALL_DISTANCE));
            end = new Vector2(Random.Range(MIN_WALL_DISTANCE, width - MIN_WALL_DISTANCE), Random.Range(MIN_WALL_DISTANCE, height - MIN_WALL_DISTANCE));
        }
        List<ConectionRoom> currentRooms = new List<ConectionRoom>();

        List<Room> rooms = RoomManager.Ins.GetLevelRoom(level);

        ConectionRoom startRoom = createStartRoom(start, rooms);
        ConectionRoom endRoom = createEndRoom(end, rooms);
        currentRooms.Add(startRoom);
        currentRooms.Add(endRoom);

        bool conectedToEndRoom = false;
        Vector2 lastPos = start;
        direction lastDirection = direction.none;
        ConectionRoom lastRoom = this.rooms[(int)start.x, (int)start.y];

        float currentDistance = Vector2.Distance(start, end);

        int looped = -1;
        bool error = false;

        while (!conectedToEndRoom)
        {
            looped++;
            if (looped >= 100)
            {
                error = true;
                break;
            }
            direction d = nextDerection(lastDirection);
            Vector2 newPosition = GetVector2ByDirection(d) + lastPos;
            if (
                // Be sure that you dont go of the map
                (newPosition.x < 0 || newPosition.y < 0) ||
                (newPosition.x >= Width || newPosition.y >= Height)
                )
            {
                continue;
            }
            else
            {
                if (newPosition == end)
                    conectedToEndRoom = true;
                if (Vector2.Distance(newPosition, end) < currentDistance)
                {
                    if (this.rooms[(int)newPosition.x, (int)newPosition.y] == null)
                    {
                        List<Room> nonBoss = rooms.Where(rm => !rm.BossRoom).ToList();
                        ConectionRoom r = new ConectionRoom(nonBoss[Random.Range(0, nonBoss.Count)], (int)newPosition.x, (int)newPosition.y);
                        this.rooms[(int)newPosition.x, (int)newPosition.y] = r;

                        lastRoom.conectedRooms.Add(r);
                        r.conectedRooms.Add(lastRoom);

                        lastRoom = r;
                        currentRooms.Add(r);

                        currentDistance = Vector2.Distance(newPosition, end);
                        lastDirection = d;
                        lastPos = newPosition;

                        looped = -1;
                    }
                }
                else
                {
                    float test = Random.Range(0, 100);
                    if (test >= 0)
                    {
                        if (this.rooms[(int)newPosition.x, (int)newPosition.y] != null)
                            continue;

                        List<Room> nonBoss = rooms.Where(rm => !rm.BossRoom).ToList();
                        ConectionRoom r = new ConectionRoom(nonBoss[Random.Range(0, nonBoss.Count)], (int)newPosition.x, (int)newPosition.y);
                        this.rooms[(int)newPosition.x, (int)newPosition.y] = r;

                        lastRoom.conectedRooms.Add(r);
                        r.conectedRooms.Add(lastRoom);

                        lastRoom = r;
                        currentRooms.Add(r);

                        currentDistance = Vector2.Distance(newPosition, end);
                        lastDirection = d;
                        lastPos = newPosition;

                        looped = -1;
                    }
                }
            }
        }

        endRoom.conectedRooms.Add(lastRoom);

        if (error)
        {
            resetRooms(level, t);
        }
        else
        {
            for (int i = 0; i < (Width * height); i++)
            {
                // createRoomBranhes(level);
            }
            displyRooms(t);
        }
    }

    /// <summary>
    /// Create side branches from the main branch
    /// </summary>
    void createRoomBranhes(int level)
    {
        List<Room> rooms = RoomManager.Ins.GetLevelRoom(level);

        ConectionRoom[,] coppyRooms = (ConectionRoom[,])this.rooms.Clone();
        ConectionRoom rstart = getRandomRoom();
        Vector2 start = new Vector2(rstart.X, rstart.Y);

        bool done = false;

        direction d = nextDerection(direction.none);
        int looped = -1;

        while (!done)
        {
            looped++;
            if (looped >= 100)
            {
                // error = true;
                break;
            }
            Vector2 newPosition = GetVector2ByDirection(d) + start;
            if (
                    // Be sure that you dont go of the map
                    (newPosition.x < 0 || newPosition.y < 0) ||
                    (newPosition.x >= Width || newPosition.y >= Height)
                    )
            {
                continue;
            }
            if (this.rooms[(int)newPosition.x, (int)newPosition.y] == null)
            {
                List<Room> nonBoss = rooms.Where(rm => !rm.BossRoom).ToList();
                ConectionRoom r = new ConectionRoom(nonBoss[Random.Range(0, nonBoss.Count)], (int)newPosition.x, (int)newPosition.y);
                this.rooms[(int)newPosition.x, (int)newPosition.y] = r;

                rstart.conectedRooms.Add(r);
                r.conectedRooms.Add(rstart);

                done = true;
            }
        }
    }

    /// <summary>
    /// Gets a random room
    /// </summary>
    /// <returns>ConectionRoom</returns>
    ConectionRoom getRandomRoom()
    {
        ConectionRoom cr = null;
        while(cr == null)
        {
            Vector2Int rndPosition = new Vector2Int(Random.Range(0, Width), Random.Range(0, height));
            if(this.rooms[rndPosition.x, rndPosition.y] != null)
            {
                if (this.rooms[rndPosition.x, rndPosition.y].BossRoom == false)
                    if (this.rooms[rndPosition.x, rndPosition.y].SideRoom == false)
                        cr = this.rooms[rndPosition.x, rndPosition.y];
            }
        }
        return cr;
    }

    /// <summary>
    /// Send to the front end to display the dungeun
    /// </summary>
    void displyRooms(System.Action<ConectionRoom> t)
    {
        for (int x = 0; x < rooms.GetLength(0); x++)
        {
            for (int y = 0; y < rooms.GetLength(1); y++)
            {
                if(rooms[x,y] != null)
                {
                    t(rooms[x, y]);
                }
            }
        }
    }

    /// <summary>
    /// Resets the dungeon
    /// </summary>
    /// <param name="level"></param>
    /// <param name="t"></param>
    void resetRooms(int level, System.Action<ConectionRoom> t)
    {
        rooms = new ConectionRoom[width, height];
        GenerateDungeon(level, t);
    }

    Vector2 GetVector2ByDirection(direction d)
    {
        if (Mathf.Abs((int)d) == 1)
            return new Vector2(0, (int)d);
        else
            return new Vector2((int)d / 2, 0);
    }

    // Be sure that the cube cant go back
    /// <summary>
    /// Get the next direction
    /// </summary>
    /// <param name="previos"></param>
    /// <returns></returns>
    direction nextDerection (direction previos)
    {
        direction d = (direction)Random.Range(-2, 3);

        bool suc = false;
        
        while(!suc)
        {
            if ((int)d == -((int)previos) || d == direction.none)
            {
                d = (direction)Random.Range(-2, 3);
            }
            else
            {
                suc = true;
            }
        }
        return d;
    }

    public static direction getDerection(Vector3 start, Vector3 end)
    {
        if (start.y - end.y > .1f)
            return direction.Up;
        else if (start.y - end.y < -.1f)
            return direction.down;

        if (start.x - end.x > .1f)
            return direction.right;
        if (start.x - end.x < -.1f)
            return direction.left;

        return direction.none;
    }

    ConectionRoom createStartRoom(Vector2 start, List<Room> rooms)
    {
        List<Room> nonBoss = rooms.Where(rm => !rm.BossRoom).ToList();
        ConectionRoom r = new ConectionRoom(nonBoss[Random.Range(0, nonBoss.Count)], (int)start.x, (int)start.y);
        r.start = true;
        this.rooms[(int)start.x, (int)start.y] = r;
        return r;
    }

    ConectionRoom createEndRoom(Vector2 end, List<Room> rooms)
    {
        List<Room> Boss = rooms.Where(rm => (rm.BossRoom)).ToList();
        ConectionRoom r = new ConectionRoom(Boss[Random.Range(0, Boss.Count)], (int)end.x, (int)end.y);
        r.BossRoom = true;
        this.rooms[(int)end.x, (int)end.y] = r;
        return r;
    }

    public ConectionRoom GetRoom(Vector2 position)
    {
        return null;
    }

    public void SaveJson()
    {
        List<ConectionRoom> conectionRooms = new List<ConectionRoom>();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                conectionRooms.Add(rooms[x, y]);
            }
        }
    }

    public Room getRoom(int x, int y)
    {
        if ((x >= 0 && x < width) && (y >= 0 && y < height))
        {
            return rooms[x, y];
        }
        return null;
    }
}

public enum direction
{
    none = 0,

    Up = 1,
    down = -1,

    left = -2,
    right = 2
}
