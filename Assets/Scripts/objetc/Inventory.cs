﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory : IInventory
{
    public int MaxItems { get => maxItems; set => maxItems = value; }
    public float MaxWeight { get => maxWeight; set => maxWeight = value; }
    public List<IItem> Items { get => items; set => items = value; }

    [SerializeField]
    private List<string> itemStrings = new List<string>();
    [SerializeField]
    private int maxItems;
    [SerializeField]
    private float maxWeight;
    private List<IItem> items = new List<IItem>();
}
