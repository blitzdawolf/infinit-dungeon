﻿using MoonSharp.Interpreter;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Lua : MonoBehaviour
{
    public bool Loaded;
    public bool ReLoad;

   //  Dictionary<string, string> scripts = new Dictionary<string, string>();
    Dictionary<string, Script> scripts = new Dictionary<string, Script>();

    public Lua(string LuaCode)
    {
        Script myLuaScript = new Script();
        Loaded = true;
    }

    public void GetResult(string name, string function)
    {
        if (scripts.ContainsKey(name.ToLower()))
        {
            Script script = scripts[name.ToLower()];
            DynValue result = script.Call(script.Globals["test"]);
            switch (result.Type)
            {
                case DataType.Boolean:
                    Debug.Log(result.Boolean);
                    break;
                case DataType.Number:
                    Debug.Log(result.Number);
                    break;
                case DataType.String:
                    Debug.Log(result.String);
                    break;
                case DataType.Function:
                case DataType.Table:
                case DataType.Tuple:
                case DataType.UserData:
                case DataType.Thread:
                case DataType.ClrFunction:
                case DataType.TailCallRequest:
                case DataType.YieldRequest:
                case DataType.Nil:
                case DataType.Void:
                default:
                    break;
            }
        }
        return;
    }

    private void Start()
    {
        Load();
    }

    public void Load()
    {
        scripts = new Dictionary<string, Script>();

        object[] results = Resources.LoadAll("Lua", typeof(TextAsset));

        foreach (TextAsset ta in results.OfType<TextAsset>())
        {
            Script myLuaScript = new Script();
            DynValue result = myLuaScript.DoString(ta.text);

            scripts.Add(ta.name.ToLower(), myLuaScript);
        }

        Loaded = true;
        ReLoad = false;
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.R))
        {
            ReLoad = true;
            Load();
        }
        if (Input.GetKeyUp(KeyCode.T))
        {
            GetResult("test", "test");
        }
    }
}