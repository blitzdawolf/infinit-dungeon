﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace infdungeon.objects
{
    public class Tile
    {
        public Room room;
        public bool wall;
        public bool Door;
        public ConectionRoom nextRoom;
        public ICharacter character => ChracterManager.Ins.GetCharcacterFromTile(this, room);

        public float cost {
            get
            {
                if (character != null)
                    return 0;
                return (wall) ? 0 : 1;
            }
        }

        public int X
        {
            get;
            set;
        }

        public int Y
        {
            get;
            set;
        }

        public Vector3 Position => new Vector3(X, Y);

        public Tile[] GetNeighbours(bool diagOkay = false)
        {
            Tile[] ns;

            if(diagOkay == false)
            {
                ns = new Tile[4];
            }
            else
            {
                ns = new Tile[8];
            }

            Tile n;

            n = room.getTile(X, Y + 1);
            ns[0] = n;
            n = room.getTile(X+1, Y);
            ns[1] = n;
            n = room.getTile(X, Y - 1);
            ns[2] = n;
            n = room.getTile(X-1, Y);
            ns[3] = n;

            if (diagOkay)
            {
                n = room.getTile(X+1, Y + 1);
                ns[4] = n;
                n = room.getTile(X + 1, Y-1);
                ns[5] = n;
                n = room.getTile(X-1, Y - 1);
                ns[6] = n;
                n = room.getTile(X - 1, Y+1);
                ns[7] = n;
            }

            return ns;
        }

        public Tile()
        {

        }

        public Tile(int x,int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}