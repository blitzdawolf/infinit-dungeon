﻿using Assets.Scripts.PathFinding;
using infdungeon.objects;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Room
{
    public string Name { get => name; set => name = value; }
    public string Location { get => location; set => location = value; }
    public int StartLevel { get => startLevel; set => startLevel = value; }
    public int EndLevel { get => endLevel; set => endLevel = value; }
    public bool SideRoom { get => sideRoom; set => sideRoom = value; }
    public bool QuestRoom { get => questRoom; set => questRoom = value; }
    public bool BossRoom { get => bossRoom; set => bossRoom = value; }

    public int X => x;
    public int Y => y;

    [SerializeField]
    protected int x;
    [SerializeField]
    protected int y;

    [SerializeField]
    private string name;
    [SerializeField]
    private string location;
    [SerializeField]
    private int startLevel;
    [SerializeField]
    private int endLevel;
    [SerializeField]
    private bool sideRoom;
    [SerializeField]
    private bool questRoom;
    [SerializeField]
    private bool bossRoom;

    public int Width => tiles.GetLength(0);
    public int Height => tiles.GetLength(1);

    [NonSerialized]
    public Tile[,] tiles;

    public TileGraph tileGraph { get; set; }

    public Tile getTile(int x, int y)
    {
        if (tiles == null)
            return null;
        if ((x >= 0 && x < Width)
            &&
           (y >= 0 && y < Height))
        {
            return tiles[x, y];
        }
        return null;
    }

    public void AddAI(GameObject charactersHolder)
    {
        List<AICharacter> aicharacters = ChracterManager.Ins.GetAiCharacters();
        AICharacter aiChar = aicharacters[UnityEngine.Random.Range(0, aicharacters.Count)];
        GameObject obj = GameObject.Instantiate(aiChar.gameObject);
        aiChar.RoomPosition = new Vector2Int(UnityEngine.Random.Range(1, 21), UnityEngine.Random.Range(1, 21));
        aiChar.name = $"{this.X}, {this.Y}";
        aiChar.setRoom((Room)this);
        obj.transform.name = $"Room {aiChar.CurrentRoom.X}, {aiChar.CurrentRoom.Y}";

        obj.transform.position = new Vector3(aiChar.GlobalPosition.x, aiChar.GlobalPosition.y);
        obj.transform.parent = charactersHolder.transform;
    }

    public override string ToString()
    {
        return $"{{{Name}, {X}, {Y}}}";
    }
}
