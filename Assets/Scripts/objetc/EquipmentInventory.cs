﻿using System;
using UnityEngine;

[Serializable]
public class EquipmentInventory : IEquipmentInventory
{
    public IEquip Head
    {
        get => head; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Head)
                head = (Armour)value;
        }
    }
    public IEquip Torso
    {
        get => torso; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Torso)
                torso = (Armour)value;
        }
    }
    public IEquip Hands
    {
        get => hands; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Hands)
                hands = (Armour)value;
        }
    }
    public IEquip Legs
    {
        get => legs; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Legs)
                legs = (Armour)value;
        }
    }
    public IEquip Feet
    {
        get => feet; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Feet)
                feet = (Armour)value;
        }
    }
    public IEquip Primary
    {
        get => primary; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Primary)
                primary = (Weapon)value;
        }
    }
    public IEquip Secondary
    {
        get => secondary; set
        {
            if (value == null)
                return;
            if (value.EquipPlace == EquipPlace.Secondary)
                secondary = (Weapon)value;
        }
    }

    [SerializeField]
    private Armour head;
    [SerializeField]
    private Armour torso;
    [SerializeField]
    private Armour hands;
    [SerializeField]
    private Armour legs;
    [SerializeField]
    private Armour feet;
    [SerializeField]
    private Weapon primary;
    [SerializeField]
    private Weapon secondary;

    public Stats AddingStats()
    {
        Stats s = head.addStats;
        s += torso.addStats;
        s += hands.addStats;
        s += legs.addStats;
        s += feet.addStats;
        s += primary.addStats;
        s += secondary.addStats;

        return s;
    }
}