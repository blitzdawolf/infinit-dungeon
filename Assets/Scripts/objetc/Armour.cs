﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Armour : Item, IEquip
{
    public EquipPlace EquipPlace { get => equipPlace; set => equipPlace = value; }

    [SerializeField]
    private EquipPlace equipPlace;
}
