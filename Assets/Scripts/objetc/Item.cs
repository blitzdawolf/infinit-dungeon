﻿using System;
using Unity.Collections;
using UnityEngine;

[Serializable]
public class Item : IItem
{
    public string Name { get => name; set => this.name = value; }
    public int Amount { get => amount; set => amount = value; }
    public float Worth { get => worth; set => worth = value; }
    public bool Stackable { get => stackable; set => stackable = value; }
    public ItemType ItemType { get => itemType; set => itemType = value; }
    public string ArtPath { get => artPath; set => artPath = value; }

    public string ID { get => Id; set => Id = value; }
    public Sprite Sprite { get => sprite; set => sprite = value; }
    public string SpriteName { get => spriteName; set => spriteName = value; }
    public Stats addStats { get => AddStats; set => AddStats = value; }

    [SerializeField]
    private string name;
    [SerializeField]
    private string Id;
    [SerializeField]
    private int amount;
    [SerializeField]
    private float worth;
    [SerializeField]
    private bool stackable;
    [SerializeField]
    private ItemType itemType;
    [SerializeField]
    private string artPath;
    [SerializeField]
    private string spriteName;
    [NonSerialized]
    [SerializeField]
    private Sprite sprite;
    [SerializeField]
    private Stats AddStats;
}
