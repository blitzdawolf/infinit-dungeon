﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Tnterface for the inventory of
/// Player, chests, banks
/// </summary>
public interface IInventory
{
    int MaxItems { get; set; }
    float MaxWeight { get; set; }
    List<IItem> Items { get; set; }
}
