﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItem
{
    string ID { get; }
    string Name { get; set; }
    int Amount { get; set; }
    float Worth { get; set; }
    bool Stackable { get; set; }
    ItemType ItemType { get; set;}
    string ArtPath { get; set; }
    string SpriteName { get; set; }
    Sprite Sprite { get; set; }
    Stats addStats { get; set; }
}
