﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEquip : IItem
{
    EquipPlace EquipPlace { get; set; }
}