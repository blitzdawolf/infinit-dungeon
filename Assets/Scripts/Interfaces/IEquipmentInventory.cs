﻿/// <summary>
/// Inventory of the equiped items
/// </summary>
public interface IEquipmentInventory
{
    IEquip Head { get; }
    IEquip Torso { get; }
    IEquip Hands{ get; }
    IEquip Legs{ get; }
    IEquip Feet{ get; }
    IEquip Primary{ get; }
    IEquip Secondary{ get; }

    Stats AddingStats();
}